package org.geomundus.gmworkshop2013;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import es.uji.geotec.geomundus.client.NotesClient;
import es.uji.geotec.geomundus.common.LatLon;
import es.uji.geotec.geomundus.common.Note;
import es.uji.geotec.geomundus.common.NotesFence;

import android.os.AsyncTask;
import android.widget.Toast;

public class GetNotesTask extends AsyncTask<String, Void, List<NotesFence>> {

	private final NotesClient notes;
	private final MapActivity activity;

	public GetNotesTask(MapActivity act,NotesClient n) {
		super();
		notes=n;
		activity=act;
	}

	protected List<NotesFence> doInBackground(String... urls) {
		List<NotesFence> aux=notes.getNotesFences();
		return aux;
	}

	protected void onPostExecute(List<NotesFence> result) {
		System.out.println(result.toString());
		activity.receiveGetNotes(result);
		
	}
	
	
}