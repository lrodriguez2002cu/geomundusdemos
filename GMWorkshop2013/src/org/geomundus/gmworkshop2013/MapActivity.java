package org.geomundus.gmworkshop2013;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import es.uji.geotec.geomundus.client.NotesClient;
import es.uji.geotec.geomundus.common.LatLon;
import es.uji.geotec.geomundus.common.Note;
import es.uji.geotec.geomundus.common.NotesFence;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class MapActivity extends Activity {
	GoogleMap mMap;
	Marker marker = null;
	NotesClient notes = new NotesClient(
			"http://geo4.dlsi.uji.es/geonotes/webresources/svc1/");

	// NotesClient notes=new
	// NotesClient("http://geo4.dlsi.uji.es/geonotes/webresources/svc/");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		drawNoteOnMap(39.994547, -0.068968);
		
		/*SouthWest --> 39.98666923875774, -0.0704985111951828

nordEst -->39.998626774679735,  -0.054740868508815765
		 * */
		
		LatLng SW = new LatLng( 39.98666923875774, -0.0704985111951828);
		LatLng NE =  new LatLng(39.998626774679735,  -0.054740868508815765);
		
		addOverlay(NE, SW);

		ImageButton btnAdd = (ImageButton) findViewById(R.id.NewNote);
		ImageButton btnShow = (ImageButton) findViewById(R.id.ShowNotes);

		btnAdd.setOnClickListener(qdb);
		btnShow.setOnClickListener(qdb2);
	}

	private OnClickListener qdb = new OnClickListener() {
		public void onClick(View v) {
			addNote();
		}
	};
	private OnClickListener qdb2 = new OnClickListener() {
		public void onClick(View v) {
			getNotes();
		}
	};

	public void addNote() {
		notes.baseURL = "http://geo4.dlsi.uji.es/geonotes/webresources/svc/";
		new AddTask(this, new LatLon(marker.getPosition().latitude,
				marker.getPosition().longitude), notes).execute();
	}

	public void receiveAddNote(Note n) {
		drawNoteOnMap(n.getLocation().getLat(), n.getLocation().getLng(),
				R.drawable.note);

	}

	public void getNotes() {
		new GetNotesTask(this, notes).execute();
	}

	public void receiveGetNotes(List<NotesFence> nf) {
		for (NotesFence n : nf) {

			List<LatLon> points = n.getGeom();
			List<LatLng> googlePoints = Utils.convertToGoogleLatLng(points);

			Polygon polygon = mMap.addPolygon(new PolygonOptions().strokeColor(
					Color.RED).fillColor(Color.BLUE));
			polygon.setPoints(googlePoints);
		}

	}

	public void addOverlay(LatLng NE, LatLng SW) {
		
		LatLngBounds ujiBounds = new LatLngBounds(SW, NE);
		GroundOverlayOptions newarkMap = new GroundOverlayOptions().image(
				BitmapDescriptorFactory.fromResource(R.drawable.map_image))
				.positionFromBounds(ujiBounds);
		
		// Add an overlay, retaining a handle to the GroundOverlay object.
		GroundOverlay imageOverlay = mMap.addGroundOverlay(newarkMap);
		
		//possibly do something with the ground Overlay

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	public void drawNoteOnMap(double x, double y) {
		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();

		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(x, y),
				14.0f));

		marker = mMap.addMarker(new MarkerOptions().position(new LatLng(x, y))
				.title("Your position").draggable(true)
				.snippet("(" + x + ";" + y + ")"));
	}

	public void drawNoteOnMap(double x, double y, int markerId) {
		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();
		mMap.addMarker(new MarkerOptions().position(new LatLng(x, y))
				.title("Your position").snippet("(" + x + ";" + y + ")")
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.note)));
	}
}
