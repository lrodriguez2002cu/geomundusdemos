package org.geomundus.gmworkshop2013;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import es.uji.geotec.geomundus.client.NotesClient;
import es.uji.geotec.geomundus.common.LatLon;
import es.uji.geotec.geomundus.common.Note;

import android.os.AsyncTask;
import android.widget.Toast;

public class AddTask extends AsyncTask<String, Void, Note> {

	private final LatLon position;
	private final NotesClient notes;
	private final MapActivity activity;

	public AddTask(MapActivity act,LatLon pos,NotesClient n) {
		super();
		this.position=pos;
		this.notes=n;
		this.activity=act;
	}

	protected Note doInBackground(String... urls) {
		Note aux=notes.addNote(position);
		return aux;
	}

	protected void onPostExecute(Note result) {
		System.out.println(result.toString());
		activity.receiveAddNote(result);
	}
	
	
}