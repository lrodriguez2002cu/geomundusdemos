package es.uji.geotec.geomundus.client;

import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import es.uji.geotec.geomundus.common.LatLon;
import es.uji.geotec.geomundus.common.Note;
import es.uji.geotec.geomundus.common.NoteEntry;
import es.uji.geotec.geomundus.common.NotesFence;
import es.uji.geotec.geomundus.common.NotesServicesInterface;
import es.uji.geotec.geomundus.common.User;

public class NotesClient extends BaseService implements NotesServicesInterface {

	public String baseURL= "";
	
	public void setBaseURL(String url){
		this.baseURL = url;
	}
	
	public NotesClient(String baseURL) {
		this.baseURL = baseURL;
	}
	
	public URI getURL(String append){
		if (append.startsWith("/")){
			append = append.substring(1, append.length());
		}
		String url = baseURL + append;
		return URI.create(url); 
	}
	
	/**
	 * @GET // @Path("/notes") // @Produces(MediaType.APPLICATION_JSON)
	 */
	@Override
	public List<Note> getNotes(/* @QueryParam("radius") */int radius,
	/* @QueryParam("location") */String location) {
		URI url = getURL("/notes");
		BaseResponse response = super.executeService(HttpMethod.GET, url, null);
		Gson gson = new Gson();
		
		Type collectionType = new TypeToken<ArrayList<Note>>(){}.getType();
		List<Note> result = gson.fromJson(new InputStreamReader(response.getInputStream()), collectionType);
		//List<Note> result = (List<Note>) gson.fromJson(new InputStreamReader(response.getInputStream()), Note.class);
		return result;
	}

	private static <T> T decodeJSON(BaseResponse response, Class <T> clazz){
		Gson gson = new Gson();
		T result = gson.fromJson(new InputStreamReader(response.getInputStream()), clazz);
		return result;
	}

//	private static <T> List<T> decodeJSONAsList(BaseResponse response, Class <T> clazz){
//		Gson gson = new Gson();
//		Type collectionType = new TypeToken<ArrayList<T>>(){}.getType();
//		List<T> result = gson.fromJson(new InputStreamReader(response.getInputStream()), collectionType);
//		return result;
//	}
	
	
	private String encodeJson(Object o){
		Gson gson = new Gson();
		String body = gson.toJson(o);
		return body;
	}	

	/**
	 * @POST // @Path("/notes/addentry/{id}") //
	 * @Produces(MediaType.APPLICATION_JSON) //
	 * @Consumes(MediaType.APPLICATION_JSON)
	 */
	@Override
	public Note addNoteEntry(String noteId, NoteEntry noteEntry) {
		URI url = getURL("/notes/addentry/"+ noteId);
		String body = encodeJson(noteEntry);
		BaseResponse response = super.executeServicePost( url, body);
		Note note = decodeJSON(response, Note.class);
		return note;
	}

	/**
	 * @POST // @Path("/notes/add") // @Produces(MediaType.APPLICATION_JSON) //
	 * @Consumes(MediaType.APPLICATION_JSON)
	 */
	@Override
	public Note addNote(LatLon location) {
		URI url = getURL("/notes/add");
		String body = encodeJson(location);
		BaseResponse response = super.executeServicePost(url, body);
        Note result = decodeJSON(response, Note.class);
		return result;
	}

	/**
	 * @DELETE // @Path("/notes/{id}") // @Produces(MediaType.APPLICATION_JSON)
	 */
	@Override
	public Note removeNote(String noteId) {
		URI url = getURL("/notes/"+noteId);
		BaseResponse response = super.executeService(HttpMethod.DELETE, url, null);
        Note result = decodeJSON(response, Note.class);
		return result;
	}

	/**
	 * @POST // @Path("/user/position/{id}") //
	 * @Produces(MediaType.APPLICATION_JSON) //
	 * @Consumes(MediaType.APPLICATION_JSON)
	 */
	@Override
	public List<Note> updateUserPosition(String userId, LatLon userPosition) {
		URI url = getURL("/user/position/"+userId);
		String body = encodeJson(userPosition);
		BaseResponse response = super.executeServicePost(url, body);
		
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<Note>>(){}.getType();
		List<Note> result = gson.fromJson(new InputStreamReader(response.getInputStream()), collectionType);
        //List<Note> result = decodeJSONAsList(response, Note.class);
		return result;
	}

	/**
	 * @POST // @Path("/adduser/") // @Produces(MediaType.APPLICATION_JSON) //
	 * @Consumes(MediaType.APPLICATION_JSON)
	 */
	@Override
	public User addUser(User user) {
		URI url = getURL("/adduser/");
		String body = encodeJson(user);
		BaseResponse response = super.executeServicePost(url, body);
        User result = decodeJSON(response, User.class);
		return result;
	}

	/**
	 * @GET // @Path("/users") // @Produces(MediaType.APPLICATION_JSON)
	 */
	@Override
	public List<User> getUsers() {
		URI url = getURL("/users");
		BaseResponse response = super.executeService(HttpMethod.GET, url, null);
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<User>>(){}.getType();
		List<User> result = gson.fromJson(new InputStreamReader(response.getInputStream()), collectionType);
        //List<User> result = decodeJSONAsList(response, User.class);
		return result;
	}

	/**	@GET
//	@Path("/notesfences")
//	@Produces(MediaType.APPLICATION_JSON)*/
	@Override
	public List<NotesFence> getNotesFences() {
		URI url = getURL("/notesfences");
		BaseResponse response = super.executeService(HttpMethod.GET, url, null);
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<NotesFence>>(){}.getType();
		List<NotesFence> result = gson.fromJson(new InputStreamReader(response.getInputStream()), collectionType);
		return result;
		
//        List<NotesFence> result = decodeJSONAsList(response, NotesFence.class);
//		return result;
	}

}
