package es.uji.geotec.geomundus.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class BaseService {

	HttpClient httpClient;

	private boolean isParametrized(String url) {
		return url.matches("/:*$");
	}

	// protected URI prepareURL() {
	//
	// if (infos.size() > 0) {
	// idParam = infos.get(0);
	// if (/*
	// * !allowNullIds &&
	// */(idParam == null || idParam.getValue() == null)
	// && isParametrized(resultUrl)) {
	// throw new RuntimeException(String.format(
	// "this url '%s' requires a valid id param", resultUrl));
	// }
	//
	// if (idParam != null && idParam.getValue() != null) {
	// resultUrl = resultUrl.replace(":id", idParam.getValue()
	// .toString());
	// }
	// }
	//
	// try {
	// URI resultURI = new URI(resultUrl);
	// return resultURI;
	// } catch (URISyntaxException e) {
	// throw new RuntimeException(e);
	// }
	// }

	public static enum HttpMethod {
		GET, POST, DELETE, PUT
	}

	private HttpRequestBase configureHttpClient(HttpMethod supportedMethod,
			URI url, String body) throws UnsupportedEncodingException {

		httpClient = new DefaultHttpClient();
		// HttpMethod supportedMethod = params.getUseMethod();
		HttpRequestBase httpCommand = null;

		switch (supportedMethod) {
		case GET:
			httpCommand = new HttpGet(url);
			break;
		case POST:
			httpCommand = new HttpPost(url);
			break;

		case DELETE:
			httpCommand = new HttpDelete(url);
			break;
		case PUT:
		default:
			throw new RuntimeException("Illegal API Http method."
					+ supportedMethod.name());
		}

		if (supportedMethod == HttpMethod.POST) {
			prepareRequestBody(body, httpCommand);
		}/*
		 * else { if (supportedMethod == HttpMethod.GET)
		 * prepareRequestURLParams(body, httpCommand); }
		 */
		return httpCommand;
	}

	protected void prepareRequestBody(String body, HttpRequestBase httpCommand)
			throws UnsupportedEncodingException {
		if (httpCommand instanceof HttpPost) {
			httpCommand.addHeader("Content-Type", "application/json");
			StringEntity entity;
			entity = new StringEntity(body, "UTF-8");
			((HttpPost) httpCommand).setEntity(entity);
		}
	}

	protected BaseResponse executeServicePost(URI url, String body) {
		return executeService(HttpMethod.POST, url, body);
	}

	protected BaseResponse executeService(HttpMethod method, URI url,
			String body) {
		try {
			return execute(method, url, body);
		} catch (Exception e) {
			System.out.println("Error:" + e.getMessage());
			throw new RuntimeException(e);
		}
	}

	
	// convert InputStream to String
	private static String getStringFromInputStream(InputStream is) {
 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
 
	}
	
	private BaseResponse execute(HttpMethod method, URI url, String body)
			throws ClientProtocolException, IOException {

		HttpRequestBase httpCommand = configureHttpClient(method, url, body);

		HttpResponse response = httpClient.execute(httpCommand);

		int statusCode = response.getStatusLine().getStatusCode();
		HttpEntity entity = response.getEntity();

		InputStream responseContent = null;
		if (entity != null) {
			responseContent = entity.getContent();
//			String content = getStringFromInputStream(responseContent);
//			System.out.println(content);
		}
		return new BaseResponse(statusCode, responseContent);
	}

	public static class BaseResponse {
		private InputStream inputStream;
		private int statusCode;

		public BaseResponse(int statusCode, InputStream responseContent) {
			this.setInputStream(responseContent);
			this.setStatusCode(statusCode);
		}

		public int getStatusCode() {
			return statusCode;
		}

		public void setStatusCode(int statusCode) {
			this.statusCode = statusCode;
		}

		public InputStream getInputStream() {
			return inputStream;
		}

		public void setInputStream(InputStream inputStream) {
			this.inputStream = inputStream;
		}
	}

}
