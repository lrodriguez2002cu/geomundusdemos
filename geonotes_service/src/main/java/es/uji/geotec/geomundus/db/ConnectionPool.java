

package es.uji.geotec.geomundus.db;

import java.sql.*;
import java.util.*;
import java.io.*;


public class ConnectionPool
{

   private String name;
   private String URL;
   private String user;
   private String password;
   private int maxConns;
   private int timeOut;

   private int checkedOut;
   private Vector<Connection> freeConnections = new Vector<Connection>();

   //private Logger log = LogManager.getLogManager().getLogger(ConnectionPool.class.getName());  


   public ConnectionPool(String name, String URL, String user,
      String password, int maxConns, int initConns, int timeOut)
      {

      this.name = name;
      this.URL = URL;
      this.user = user;
      this.password = password;
      this.maxConns = maxConns;
      this.timeOut = timeOut > 0 ? timeOut : 5;      
      initPool(initConns);

      //log.debug("New pool created");
      String lf = System.getProperty("line.separator");
      /*log.debug(lf +
                    " url=" + URL + lf +
                    " user=" + user + lf +
                    " password=" + password + lf +
                    " initconns=" + initConns + lf +
                    " maxconns=" + maxConns + lf +
                    " logintimeout=" + this.timeOut);
      log.debug(getStats());*/
   }

   private void initPool(int initConns)
   {
      for (int i = 0; i < initConns; i++)
      {
         try
         {
            Connection pc = newConnection();
            freeConnections.addElement(pc);
         }
         catch (SQLException e)
         { }
      }
   }

   public Connection getConnection() throws SQLException
   {
	   //log.debug("Request for connection received");
      try
      {
    	  //log.debug("getConnection thingie received");
         return getConnection(timeOut * 1000);
      }
       catch (SQLException e)
      {
    	   //log.error("Exception getting connection");
         throw e;
      }
   }

   private synchronized Connection getConnection(long timeout)
                      throws SQLException
   {
	   //log.debug("getConnection called");
      // Get a pooled Connection from the cache or a new one.
      // Wait if all are checked out and the max limit has
      // been reached.
      long startTime = System.currentTimeMillis();
      long remaining = timeout;
      Connection conn = null;
      //log.debug("getConnection called II");
      while ((conn = getPooledConnection()) == null)
      {
    	  //log.debug("getPooledConnection==null");
         try
         {
        	 //log.debug("Waiting for connection. Timeout=" + remaining);
            wait(remaining);
         }
         catch (InterruptedException e)
         { }
         remaining = timeout - (System.currentTimeMillis() - startTime);
         if (remaining <= 0)
         {
            // Timeout has expired
        	 //log.error("Time-out while waiting for connection");
            throw new SQLException("getConnection() timed-out");
         }
      }

      //log.debug("going to call isConnectionOK");
      
      // Check if the Connection is still OK
      if (!isConnectionOK(conn))
      {
         // It was bad. Try again with the remaining timeout
    	  //log.debug("Removed selected bad connection from pool");
         return getConnection(remaining);
      }
      checkedOut++;
      //log.debug("Delivered connection from pool");
      //log.debug(getStats());
      return conn;
   }

   private boolean isConnectionOK(Connection conn)
   {
	   //log.debug("isConnectionOK?");
      Statement testStmt = null;
      try
      {
         if (!conn.isClosed())
         {
        	 //log.debug("isConnectionOK - Connection is openened?");
            
            // Try to createStatement to see if it's really alive
            testStmt = conn.createStatement();

            //log.debug("isConnectionOK - created statement");
            testStmt.close();
            //log.debug("isConnectionOK - closed statement");
         }
         else
         {
        	 //log.debug("isConnectionOK - connection closed");
            return false;
         }
      }
      catch (SQLException e)
      {
         if (testStmt != null)
         {
            try
            {
               testStmt.close();
            }
            catch (SQLException se)
            { }
         }
         //log.debug("Pooled Connection was not okay");
         return false;
      }
      return true;
   }

   private Connection getPooledConnection() throws SQLException
   {
      Connection conn = null;
      if (freeConnections.size() > 0)
      {
         // Pick the first Connection in the Vector
         // to get round-robin usage
         conn = (Connection) freeConnections.firstElement();
         freeConnections.removeElementAt(0);
      }
      else if (maxConns == 0 || checkedOut < maxConns)
      {
         conn = newConnection();
      }
      return conn;
   }

   private Connection newConnection() throws SQLException
   {
      Connection conn = null;
      if (user == null)
      {
         conn = DriverManager.getConnection(URL);
      }
      else
      {
         conn = DriverManager.getConnection(URL, user, password);
      }
      //log.debug("Opened a new connection");
      return conn;
   }

   public synchronized void freeConnection(Connection conn)
   {
      // Put the connection at the end of the Vector
      freeConnections.addElement(conn);
      checkedOut--;
      notifyAll();
      //log.debug("Returned connection to pool");
      //log.debug(getStats());
   }

   public synchronized void release()
   {
      Enumeration allConnections = freeConnections.elements();
      while (allConnections.hasMoreElements())
      {
         Connection con = (Connection) allConnections.nextElement();
         try
         {
            con.close();
            //log.debug("Closed connection");
         }
         catch (SQLException e)
         {
        	 //log.error("Couldn't close connection");
         }
      }
      freeConnections.removeAllElements();
   }

   private String getStats() {
      return "Total connections: " +
         (freeConnections.size() + checkedOut) +
         " Available: " + freeConnections.size() +
         " Checked-out: " + checkedOut;
   }


}