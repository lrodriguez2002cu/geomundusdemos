package es.uji.geotec.geomundus.db;


import java.util.Properties;
import java.sql.*;

import es.uji.geotec.geomundus.db.PropertyManager;

/**
 * Title:         ConnectionManager
 * Description:   This class implements a tool to manage the connections
 * @author        Laura Diaz
 * @version       1.0
 */
public class ConnectionManager
{
  private boolean m_usePool = false;
  private String m_connectionName = null;
  private String m_user = null;
  private String m_passwd = null;
  private String m_url = null;
  private String m_driver = null;

  boolean CLOSE_CONNECTION_SUCCESS = true;
  boolean CLOSE_CONNECTION_FAILURE = false;
	
/**
* Default constructor.
* Reading the connection property file will initialize the class members.
*/
  public ConnectionManager()
  {
    try
    {
      Properties dbProps = PropertyManager.getProperties(PropertyManager.DB_PROPERTIES);
      String use_pool = dbProps.getProperty("usepool","");
      if(use_pool.equalsIgnoreCase("true"))
      {
        m_usePool = true;
      }
      m_user = dbProps.getProperty("user","");
      m_passwd = dbProps.getProperty("password","");
      m_url = dbProps.getProperty("url","");
      m_driver = dbProps.getProperty("drivers","");
      m_connectionName = dbProps.getProperty("connection_name","");
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

 /**
  * Gets a connection, depending on the values read, it will use
  * the pool or it will get use a normal connection
  *
  * @return Connection: the connection
  */
  public Connection getConnection()
  {
    Connection conn = null;
    if(m_usePool){
      if ( (m_connectionName == null)
        || (m_connectionName.length() == 0) ){
        conn = PoolManager.getInstance().getConnection();
      }else{
        conn = PoolManager.getInstance().getConnection(m_connectionName);
      }
    }else{
      try{
        if (m_user == null){
          Driver driver = (Driver)Class.forName(m_driver).newInstance();
          DriverManager.registerDriver(driver);
          conn = DriverManager.getConnection(m_url);
        }else{
          conn = DriverManager.getConnection(m_url, m_user, m_passwd);
        }
      }catch(Exception e){
        e.printStackTrace();
        return null;
      }
    }
    return conn;
  }

 /**
  * Fress a connection
  *
  * @param Connection: the connection
  */
  public boolean freeConnection(Connection con)
  {
    if(m_usePool){
      if ( (m_connectionName == null)
        || (m_connectionName.length() == 0) ){
        PoolManager.getInstance().freeConnection(con);
        return CLOSE_CONNECTION_SUCCESS;
      }else{
        PoolManager.getInstance().freeConnection(m_connectionName,con);
        return CLOSE_CONNECTION_SUCCESS;
      }
    }else{
      try{
        con.close();
        return CLOSE_CONNECTION_SUCCESS;
      }
      catch(Exception e){
        e.printStackTrace();
        return CLOSE_CONNECTION_FAILURE;
      }
    }
  }

}