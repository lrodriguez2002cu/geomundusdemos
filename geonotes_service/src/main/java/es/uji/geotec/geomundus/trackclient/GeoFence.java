package es.uji.geotec.geomundus.trackclient;

import java.util.ArrayList;
import java.util.List;

/**
 * Geofences
 * 
 * Overview Creating geofences Adding members to a geofence Removing members
 * from a geofence Listing geofences Requesting specific geofences Requesting a
 * contiguous list of geofences Deleting geofences Retrieving active geofences
 * Overview
 * 
 * Developers � Google Maps API � Web iOS Android Showcase Support Blog GDG Live
 * A geofence defines a region of interest. For example, this may be a polygon
 * outside of which a particular set of entities is not expected to stray.
 * Geofences apply to particular collections.
 * 
 * Each geofence has a number of properties:
 * 
 * ID: An opaque string used to refer to the geofence in calls to various
 * methods. The ID for a geofence is assigned by the API at creation time. There
 * are no guarantees about its form except that it will be unique across
 * geofences for a particular Service Account client ID.
 * 
 * Name: A user-defined string describing the geofence. You may use this for any
 * purpose; for example, recording a human-readable name to show to your
 * applications' end users. Names must consist of Unicode text and need not be
 * unique. The name for a geofence may be left unspecified.
 * 
 * Collection IDs: Zero or more IDs of collections to which the geofence
 * applies. Initially the geofence is empty, but you can make API calls to add
 * and remove collections.
 * 
 * Polygon: A polygon specifying the geofence's region. Crumbs recorded for
 * entities belonging to one of the geofence's collections that fall within this
 * polygon make the geofence active. This is required.
 * 
 * @author lrodriguez
 * 
 */
public class GeoFence {
	/**
	 * ID: An opaque string used to refer to the geofence in calls to various
	 * methods. The ID for a geofence is assigned by the API at creation time.
	 * There are no guarantees about its form except that it will be unique
	 * across geofences for a particular Service Account client ID.
	 */
	private String id;

	/**
	 * Name: A user-defined string describing the geofence. You may use this for
	 * any purpose; for example, recording a human-readable name to show to your
	 * applications' end users. Names must consist of Unicode text and need not
	 * be unique. The name for a geofence may be left unspecified.
	 */
	private String name;

	/**
	 * Polygon: A polygon specifying the geofence's region. Crumbs recorded
	 * for entities belonging to one of the geofence's collections that fall
	 * within this polygon make the geofence active. This is required.
	 */
	private Geometry polygon;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Geometry getPolygon() {
		return polygon;
	}

	public void setPolygon(Geometry polygon) {
		this.polygon = polygon;
	}
	
	public GeoFence() {
		// TODO Auto-generated constructor stub
	}
	
	public GeoFence(String name, Geometry geom) {
		this.setName(name);
		this.setPolygon(geom);
	}
	
	public static class GeoFencesList {
		
		private List<GeoFence> geofences;
		
		public static List <String> getGeoFenceIds(List<GeoFence> geofences) {
			List <String> ids = new ArrayList<String>();
			for (GeoFence  geofence : geofences){
				ids.add(geofence.getId()); 	
			}
			return ids;
		}
	
		public static GeoFence  getGeoFenceByName(List<GeoFence> geofences, String name) {
			for (GeoFence  geofence : geofences){
				if (geofence.getName().equalsIgnoreCase(name)){
					return geofence;
				}
			}
			return null;
		}

		
		public static GeoFence getGeoFenceById(List<GeoFence> geofences, String id) {
			for (GeoFence  geofence : geofences){
				if (geofence.getId().equalsIgnoreCase(id)){
					return geofence;
				}
			}
			return null;
		}

		
		
		public List<GeoFence> getGeofences() {
			return geofences;
		}
		
		public void setGeofences(List<GeoFence> geofences) {
			this.geofences = geofences;
		}
	}
	
	@Override
	public String toString() {
		return String.format("GeoFence (name: %s, id: %s, geom: %s)", name, id, polygon.toString());
	}
}
