package es.uji.geotec.geomundus.trackclient;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.gson.Gson;


import es.uji.geotec.geomundus.trackclient.Entity.EntityIds;
import es.uji.geotec.geomundus.trackclient.Entity.EntityList;
import es.uji.geotec.geomundus.trackclient.EntityCollection.CollectionList;
import es.uji.geotec.geomundus.trackclient.EntityCollection.CollectionNames;
import es.uji.geotec.geomundus.trackclient.GeoFence.GeoFencesList;

import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

public class TracksClient {

	/**
	 * Make a request to Tracks API. Command Line Usage Examples (with Maven)
	 * 
	 * <pre>
	 * {@code
	 * mvn -q exec:java -Dexec.args="METHOD JSON_REQUEST_BODY"
	 * mvn -q exec:java -Dexec.args="crumbs/getlocationinfo {'entityId':'280415822391405995','timestamp':'1334643465000000'}"
	 * mvn -q exec:java -Dexec.args="entities/list ''"
	 * mvn -q exec:java -Dexec.args="entities/create {'entities':[{'name':'auto001','type':'AUTOMOBILE'}]}"}
	 * </pre>
	 * 
	 */
	private static TracksClient tracksClient;

	public static TracksClient getTrackClient() {
		if (tracksClient == null) {
			tracksClient = new TracksClient();
		}
		return tracksClient;
	}

	/** Global instance of the HTTP transport. */
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();

	GoogleCredential credential;

	public static class TracksServiceAccountInfo {

		/** E-mail address of the service account. */
		private static final String SERVICE_ACCOUNT_EMAIL = "728740201176-sasmec80beacmfkbnnigtb9pl84noi1o@developer.gserviceaccount.com";

		/** Global configuration of OAuth 2.0 scope. */
		private static final String TRACKS_SCOPE = "https://www.googleapis.com/auth/tracks";

		/** Global configuration for location of private key file. */
		private static final String PRIVATE_KEY_PATH = "/uji/geotec/geomundus/conf/";
		private static final String PRIVATE_KEY = "57f69d7abecea434367355723085650796d974e0-privatekey.p12";

		// private static final String CLIENT_ID =
		// "728740201176.apps.googleusercontent.com";
		// private static final String CLIENT_SECRET =
		// "x6cVyZ4WOV9T5j2w86f6dlRD";
	}

	public static String BASE_URL = "https://www.googleapis.com/tracks/v1/";

	public InputStream callMethod(String method, String requestBody)
			throws IOException, GeneralSecurityException, URISyntaxException {
		// build the credentials
		buildCredentials();

		HttpRequestFactory requestFactory = HTTP_TRANSPORT
				.createRequestFactory(credential);

		String URI = BASE_URL + method;

		GenericUrl url = new GenericUrl(URI);
		HttpRequest request = requestFactory.buildPostRequest(url,
				ByteArrayContent.fromString(null, requestBody));
		request.getHeaders().setContentType("application/json");

		// Google servers will fail to process a POST/PUT/PATCH unless the
		// Content-Length
		// header >= 1
		// request.setAllowEmptyContent(false);

		HttpResponse shortUrl = request.execute();

		// Print response.
		// BufferedReader output = new BufferedReader(new InputStreamReader(
		// shortUrl.getContent()));

		return shortUrl.getContent();

		// for (String line = output.readLine(); line != null; line = output
		// .readLine()) {
		// System.out.println(line);
		// }
	}

	public void buildCredentials() throws GeneralSecurityException,
			IOException, URISyntaxException {

		if (credential == null) {
			List<String> scopes = new ArrayList<String>();
			// InputStream is = this
			// .getClass()
			// /*.getClassLoader()*/
			// .getResourceAsStream(
			// TracksServiceAccountInfo.PRIVATE_KEY_PATH
			// + TracksServiceAccountInfo.PRIVATE_KEY);
			// is.available();

			URL url = this.getClass()
			/* .getClassLoader() */
			.getResource(
					TracksServiceAccountInfo.PRIVATE_KEY_PATH
							+ TracksServiceAccountInfo.PRIVATE_KEY);

			scopes.add(TracksServiceAccountInfo.TRACKS_SCOPE);
			// Build service account credential.
			credential = new GoogleCredential.Builder()
					.setTransport(HTTP_TRANSPORT)
					.setJsonFactory(JSON_FACTORY)
					.setServiceAccountId(
							TracksServiceAccountInfo.SERVICE_ACCOUNT_EMAIL)
					.setServiceAccountScopes(scopes)
					.setServiceAccountPrivateKeyFromP12File(
							new File(url.toURI())).build();
		}
	}

	public static class Utils {

		private static String createRequestBodyForObj(Object params)
				throws IOException {
			Gson gson = new Gson();
			String requestBody = gson.toJson(params);
			return requestBody;
		}
		
	}

	public static class BaseService {

		protected static boolean debug = false;

		public static boolean checkEmpty(InputStream result) {
			String str = getStringFromInputStream(result);
			return str.equalsIgnoreCase("{}");
		}

		public static InputStream callMethod(String methodName, String body)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			TracksClient client = TracksClient.getTrackClient();
			InputStream result = null;

			result = client.callMethod(methodName, body);

			// if (!debug) {
			// } else {
			// String str = "{ \"entityIds\": [  \"d3051ce6f465d7c6\" ]}";//
			// getStringFromInputStream(result);
			// System.out.println(str);
			// result = new ByteArrayInputStream(str.getBytes());
			// }
			return result;
		}
	}

	private static String getStringFromInputStream(InputStream is) {
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();

	}

	public static class EntitiesServices extends BaseService {

		public static final String CREATE_SVC_MTHD = "entities/create";
		public static final String LIST_SVC_MTHD = "entities/list";
		public static final String DELETE_SVC_MTHD = "entities/delete";

		public static List<Entity> createEntity(List<Entity> entities)
				throws IOException, GeneralSecurityException,
				URISyntaxException {

			String requestBody = createRequestBody(entities);
			InputStream result = callMethod(CREATE_SVC_MTHD, requestBody);
			List<String> ids = createResultIds(result);
			int i = 0;
			for (Entity ent : entities) {
				ent.setId(ids.get(i++));
			}
			return entities;
		}

		private static List<String> createResultIds(InputStream result)
				throws IOException {
			Gson gson = new Gson();
			EntityListOfIds entities = gson.fromJson(new InputStreamReader(
					result), EntityListOfIds.class);
			return entities.getEntityIds();
		}

		public static class EntityListOfIds {

			private List<String> entityIds;

			public EntityListOfIds() {
				setEntityIds(new ArrayList<String>());
			}

			public List<String> getEntityIds() {
				return entityIds;
			}

			public void setEntityIds(List<String> entityIds) {
				this.entityIds = entityIds;
			}
		}

		public static class ListEntitiesParams {

			private String minId;
			private List<String> entityIds = new ArrayList<String>();

			public String getMinId() {
				return minId;
			}

			public void setMinId(String minId) {
				this.minId = minId;
			}

			public List<String> getEntityIds() {
				return entityIds;
			}

			public void setEntityIds(List<String> entityIds) {
				this.entityIds = entityIds;
			}
		}

		public static List<Entity> getAllEntities() throws IOException,
				GeneralSecurityException, URISyntaxException {

			String requestBody = Utils
					.createRequestBodyForObj(new ListEntitiesParams());
			InputStream result = callMethod(LIST_SVC_MTHD, requestBody);
			List<Entity> entities = createResultEntityListArray(result);
			return entities;
		}

		public static boolean deleteEntities(List<Entity> entities)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String requestBody = createRequestIdsBody(entities);
			System.out.println(requestBody);
			InputStream result = callMethod(DELETE_SVC_MTHD, requestBody);
			boolean res = checkEmpty(result);
			return res;
		}

		private static String createRequestBody(List<Entity> entities)
				throws IOException {
			// JacksonFactory fact = new JacksonFactory();
			Writer stringWriter = new StringWriter();
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(stringWriter, new CreateEntityParams(entities));
			// JsonGenerator gen = fact.createJsonGenerator(stringWriter);
			// gen.serialize(entities);
			// gen.flush();
			String requestBody = stringWriter.toString();
			return requestBody;
		}

		public static class CreateEntityParams {
			private List<Entity> entities = null;

			public CreateEntityParams(List<Entity> entities) {
				this.setEntities(entities);
			}

			public List<Entity> getEntities() {
				return entities;
			}

			public void setEntities(List<Entity> entities) {
				this.entities = entities;
			}
		}

		private static String createRequestIdsBody(List<Entity> entities)
				throws IOException {
			EntityIds ids = new Entity.EntityIds(entities);
			Gson gson = new Gson();
			String requestBody = gson.toJson(ids);
			return requestBody;
		}

		private static List<Entity> createResultEntityListArray(
				InputStream stream) throws IOException {
			Gson gson = new Gson();
			EntityListResult result = gson.fromJson(new InputStreamReader(
					stream), EntityListResult.class);
			List<Entity> entities = result.getEntities();
			return entities;
		}

		public static class EntityListResult {
			private List<Entity> entities = null;

			public EntityListResult() {
				entities = new ArrayList<Entity>();
			}

			public List<Entity> getEntities() {
				return entities;
			}

			public void setEntities(List<Entity> entities) {
				this.entities = entities;
			}

		}

	}

	public static class CollectionsServices extends BaseService {

		public static final String CREATE_SVC_METHOD = "collections/create";
		public static final String ADD_ENTITY_SVC_METHOD = "collections/addentities";
		private static final String REMOVE_ENTITY_SVC_METHOD = "collections/removeentities";
		private static final String LIST_SVC_METHOD = "collections/list";
		private static final String DELETE_SVC_METHOD = "collections/delete";

		public static List<EntityCollection> createCollections(
				List<EntityCollection> collections) throws IOException,
				GeneralSecurityException, URISyntaxException {
			String requestBody = createRequestCollectionsBody(collections);
			InputStream result = callMethod(CREATE_SVC_METHOD, requestBody);

			List<String> ids = createResultStringArray(result);
			int i = 0;
			for (EntityCollection entCol : collections) {
				entCol.setId(ids.get(i++));
			}
			return collections;
		}

		public static class ListCollectionParamsBase {

			protected List<String> collectionIds;

			public ListCollectionParamsBase() {
				// collectionsIds = new ArrayList<String>();
			}

			public ListCollectionParamsBase(List<String> collectionsIds) {
				this.collectionIds = collectionsIds;
			}

			public ListCollectionParamsBase(List<EntityCollection> collections,
					boolean dummy) {
				this.collectionIds = CollectionList
						.getCollectionsIds(collections);
			}
		}

		public static class ListCollectionsParams extends
				ListCollectionParamsBase {

			private String minId;

			public ListCollectionsParams(String minId) {
				super();
				this.setMinId(minId);
			}

			public ListCollectionsParams() {
				super();
				// setMinId("");

			}

			public String getMinId() {
				return minId;
			}

			public void setMinId(String minId) {
				this.minId = minId;
			}
		}

		public static List<EntityCollection> listCollections()
				throws IOException, GeneralSecurityException,
				URISyntaxException {

			String requestBody = Utils
					.createRequestBodyForObj(new ListCollectionsParams());
			if (debug) {
				System.out.println(requestBody);
			}

			InputStream result = callMethod(LIST_SVC_METHOD, requestBody);

			// String str = getStringFromInputStream(result);
			// System.out.println(str);

			CollectionList list = createResultCollectionList(result);
			return list.getCollections();
		}

		private static CollectionList createResultCollectionList(
				InputStream stream) throws IOException {
			Gson gson = new Gson();
			// String str = getStringFromInputStream(stream);
			// System.out.println(str);
			CollectionList entities = gson.fromJson(new InputStreamReader(
					stream), CollectionList.class);
			return entities;
		}

		private static String createRequestAddEntityBody(
				EntityCollection collection, List<Entity> entities)
				throws IOException {
			AddEntityParam param = new AddEntityParam(collection,
					EntityList.getEntitiesIds(entities));
			return Utils.createRequestBodyForObj(param);
		}

		public static EntityCollection addEntityToCollection(
				EntityCollection collection, List<Entity> entities)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String requestBody = createRequestAddEntityBody(collection,
					entities);

			InputStream result = callMethod(ADD_ENTITY_SVC_METHOD, requestBody);

			if (checkEmpty(result)) {
				collection.getEntityIds().addAll(
						EntityList.getEntitiesIds(entities));
				return collection;
			}
			return null;
		}

		public static boolean removeEntityFromCollection(
				EntityCollection collection, List<Entity> entities)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String requestBody = createRequestAddEntityBody(collection,
					entities);

			InputStream result = callMethod(REMOVE_ENTITY_SVC_METHOD,
					requestBody);

			if (checkEmpty(result)) {
				collection.getEntityIds().removeAll(
						EntityList.getEntitiesIds(entities));
				return true;
			}
			return false;
		}

		public static boolean deleteCollections(
				List<EntityCollection> collections) throws IOException,
				GeneralSecurityException, URISyntaxException {
			String requestBody = createRequestDeleteCollectionBody(collections);
			InputStream result = callMethod(DELETE_SVC_METHOD, requestBody);
			return checkEmpty(result);
		}

		private static String createRequestDeleteCollectionBody(
				List<EntityCollection> collections) throws IOException {
			ListCollectionParamsBase param = new ListCollectionParamsBase(
					collections, false);
			return Utils.createRequestBodyForObj(param);
		}

		public static class AddEntityParam {

			private String collectionId;
			private List<String> entityIds;

			public AddEntityParam(String collectionId, List<String> entityIds) {
				this.setCollectionId(collectionId);
				this.setEntityIds(entityIds);
			}

			public AddEntityParam(EntityCollection collection,
					List<String> entityIds) {
				this(collection.getId(), entityIds);
			}

			public String getCollectionId() {
				return collectionId;
			}

			public void setCollectionId(String collectionId) {
				this.collectionId = collectionId;
			}

			public List<String> getEntityIds() {
				return entityIds;
			}

			public void setEntityIds(List<String> entityIds) {
				this.entityIds = entityIds;
			}
		}

		private static String createRequestCollectionsBody(
				List<EntityCollection> collections) throws IOException {
			CollectionNames names = new CollectionNames(collections);
			return Utils.createRequestBodyForObj(names);
		}

		private static List<String> createResultStringArray(InputStream stream)
				throws IOException {
			Gson gson = new Gson();
			CollectionIds cols = gson.fromJson(new InputStreamReader(stream),
					CollectionIds.class);
			return cols.collectionIds;
		}

		public class CollectionIds {
			private List<String> collectionIds;

			public CollectionIds() {
				setCollectionIds(new ArrayList<String>());
			}

			public List<String> getCollectionIds() {
				return collectionIds;
			}

			public void setCollectionIds(List<String> collectionIds) {
				this.collectionIds = collectionIds;
			}
		}
	}

	public static class CrumbsServices extends BaseService {

		public static String RECORD_SVC_MTHD = "crumbs/record";
		public static String RECENT_SVC_MTHD = "crumbs/getrecent";
		public static String HISTORY_SVC_MTHD = "crumbs/gethistory";
		public static String SUMARIZE_SVC_MTHD = "crumbs/summarize";
		public static String GEOINFO_SVC_MTHD = "crumbs/getlocationinfo";
		public static String DELETE_SVC_MTHD = "crumbs/delete";

		public static boolean recordCrumbs(Entity entity, List<Crumb> crumbs)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String entityId = entity.getId();
			return recordCrumbs(entityId, crumbs);
		}

		public static boolean recordCrumbs(String entityId, List<Crumb> crumbs)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String requestBody = createCrumbsRecordRequestBody(entityId,
					crumbs);
			InputStream result = callMethod(RECORD_SVC_MTHD, requestBody);
			return checkEmpty(result);
		}

		public static List<EntityCrumb> recentCrumbs(EntityCollection collection)
				throws IOException, GeneralSecurityException,
				URISyntaxException {

			String requestBody = createCrumbsRecentRequestBody(collection);
			InputStream result = callMethod(RECENT_SVC_MTHD, requestBody);
			List<EntityCrumb> crumbs = createResultCrumbsList(result);
			return crumbs;
		}

		public static List<Crumb> historyCrumbs(HistoryCrumbsParams params)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String requestBody = createCrumbsHistoryRequestBody(params);
			InputStream result = callMethod(HISTORY_SVC_MTHD, requestBody);
			List<Crumb> crumbs = createResultCrumbsListForHistory(result);
			return crumbs;
		}

		public static List<Polyline> sumarizeCrumbs(SummarizeCrumbsParams params)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String requestBody = createCrumbsSummaryRequestBody(params);
			InputStream result = callMethod(SUMARIZE_SVC_MTHD, requestBody);
			List<Polyline> polyline = createResultCrumbsListForSummary(result);
			return polyline;
		}

		public static boolean deleteCrumbs(DeleteCrumbsParams params)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String requestBody = createCrumbsDeleteRequestBody(params);
			InputStream result = callMethod(DELETE_SVC_MTHD, requestBody);
			return checkEmpty(result);
		}

		// public static void getGeoLocationInfo(
		// SummarizeCrumbsParams params) throws IOException,
		// GeneralSecurityException, URISyntaxException {
		// String requestBody = createCrumbsSummaryRequestBody(params);
		// InputStream result = callMethod(GEOINFO_SVC_MTHD, requestBody);
		// List<Polyline> polyline = createResultCrumbsListForSummary(result);
		// return;
		// }

		private static String createCrumbsDeleteRequestBody(
				DeleteCrumbsParams params) throws IOException {
			return Utils.createRequestBodyForObj(params);
		}

		private static List<Polyline> createResultCrumbsListForSummary(
				InputStream stream) throws IOException {
			Gson gson = new Gson();
			SummaryCrumbsResult result = gson.fromJson(new InputStreamReader(
					stream), SummaryCrumbsResult.class);
			return result.getPolylines();
		}

		private static String createCrumbsSummaryRequestBody(
				SummarizeCrumbsParams params) throws IOException {
			return Utils.createRequestBodyForObj(params);
		}

		private static String createCrumbsHistoryRequestBody(
				HistoryCrumbsParams params) throws IOException {
			return Utils.createRequestBodyForObj(params);
		}

		private static List<EntityCrumb> createResultCrumbsList(
				InputStream stream) throws IOException {
			Gson gson = new Gson();
			RecentCrumbsResult crumbs = gson.fromJson(new InputStreamReader(
					stream), RecentCrumbsResult.class);
			return crumbs.results;
		}

		private static List<Crumb> createResultCrumbsListForHistory(
				InputStream stream) throws IOException {
			Gson gson = new Gson();
			// String str = getStringFromInputStream(stream);
			// System.out.println(str);
			HistoryCrumbsResult crumbs = gson.fromJson(new InputStreamReader(
					stream), HistoryCrumbsResult.class);
			return crumbs.crumbs;
			// GeofencesParams
		}

		private static String createCrumbsRecentRequestBody(
				EntityCollection collection) throws IOException {
			return Utils.createRequestBodyForObj(new RecentCrumbsParam(
					collection.getId()));
		}

		public static class RecordCrumbsParam {
			private String entityId;
			private List<Crumb> crumbs;

			public RecordCrumbsParam(String id, List<Crumb> crumbs) {
				this.setEntityId(id);
				this.setCrumbs(crumbs);
			}

			public String getEntityId() {
				return entityId;
			}

			public void setEntityId(String entityId) {
				this.entityId = entityId;
			}

			public List<Crumb> getCrumbs() {
				return crumbs;
			}

			public void setCrumbs(List<Crumb> crumbs) {
				this.crumbs = crumbs;
			}
		}

		public static class RecentCrumbsParam {
			private String collectionId;

			public RecentCrumbsParam(String collectionId) {
				this.setCollectionId(collectionId);
			}

			public String getCollectionId() {
				return collectionId;
			}

			public void setCollectionId(String collectionId) {
				this.collectionId = collectionId;
			}
		}

		public static class RecentCrumbsResult {

			private List<EntityCrumb> results;

			public List<EntityCrumb> getResults() {
				return results;
			}

			public void setResults(List<EntityCrumb> results) {
				this.results = results;
			}

		}

		public static class EntityCrumb {

			private String entityId;
			private Crumb crumb;

			public String getEntityId() {
				return entityId;
			}

			public void setEntityId(String entityId) {
				this.entityId = entityId;
			}

			public Crumb getCrumb() {
				return crumb;
			}

			public void setCrumb(Crumb crumb) {
				this.crumb = crumb;
			}

			@Override
			public String toString() {
				return String.format("EntityCrumb(entity: %s, crumb: %s)",
						entityId, crumb);
			}
		}

		// { "results": [ { "entityId": "40b841e4b0c58592",
		// "crumb": { "timestamp": 1.384358926E9,
		// "location": { "lat": 39.98877721725515,
		// "lng": -0.04770040512084961 },
		// "heading": 90.0, "confidenceRadius": 10.0 } } ]}

		public static class HistoryCrumbsResult {
			private List<Crumb> crumbs;

			public List<Crumb> getCrumbs() {
				return crumbs;
			}

			public void setCrumbs(List<Crumb> results) {
				this.crumbs = results;
			}
		}

		public static class SummaryCrumbsResult {
			private List<Polyline> polylines;

			public List<Polyline> getPolylines() {
				return polylines;
			}

			public void setPolylines(List<Polyline> polylines) {
				this.polylines = polylines;
			}

		}

		public static class SummarizeCrumbsParams {
			/**
			 * Required. The ID of the entity whose crumbs should be retrieved.
			 */
			private String entityId;

			/**
			 * Required. The minimum timestamp in the interval of interest.
			 */
			private double minTimestamp;

			/**
			 * Required. The maximum timestamp in the interval of interest.
			 */
			private double maxTimestamp;

			public String getEntityId() {
				return entityId;
			}

			public void setEntityId(String entityId) {
				this.entityId = entityId;
			}

			public double getMinTimestamp() {
				return minTimestamp;
			}

			public void setMinTimestamp(double minTimestamp) {
				this.minTimestamp = minTimestamp;
			}

			public double getMaxTimestamp() {
				return maxTimestamp;
			}

			public void setMaxTimestamp(double maxTimestamp) {
				this.maxTimestamp = maxTimestamp;
			}

			public SummarizeCrumbsParams(String entityId, double minTime,
					double maxTime) {
				this.entityId = entityId;
				this.minTimestamp = minTime;
				this.maxTimestamp = maxTime;
			}
		}

		public static class HistoryCrumbsParams {
			/**
			 * Required. The ID of the entity whose crumbs should be retrieved.
			 */
			private String entityId;

			/**
			 * Required. The timestamp of interest. The crumb recorded for this
			 * timestamp, if any, will be returned.
			 */
			private double timestamp;
			/**
			 * : Optional. If specified, the countBefore greatest-timestamped
			 * crumbs with timestamp less than timestamp will be returned as
			 * well.
			 */
			private int countBefore;

			/**
			 * : Optional. If specified, the countBefore least-timestamped
			 * crumbs with timestamp greater than timestamp will be returned as
			 * well.
			 */
			int countAfter;

			public String getEntityId() {
				return entityId;
			}

			public void setEntityId(String entityId) {
				this.entityId = entityId;
			}

			public double getTimestamp() {
				return timestamp;
			}

			public void setTimestamp(double timestamp) {
				this.timestamp = timestamp;
			}

			public int getCountBefore() {
				return countBefore;
			}

			public void setCountBefore(int countBefore) {
				this.countBefore = countBefore;
			}

			public HistoryCrumbsParams() {

			}

			public HistoryCrumbsParams(String entityId, double timestamp,
					int countBefore, int countAfter) {
				this.entityId = entityId;
				this.countBefore = countBefore;
				this.countAfter = countAfter;
				this.timestamp = timestamp;
			}

		}

		@SuppressWarnings("unused")
		private static class GeoLocationInfoParams {
			/**
			 * Required. The ID of the entity whose location is of interest.
			 */
			String entityId;

			/**
			 * Required. The timestamp of the location of interest. The API will
			 * search for crumbs near this time.
			 */
			long timestamp;

			/**
			 * A CLDR language identifier specifying the language with which the
			 * results should be localized. For example, en or zh-CN. This is
			 * optional.
			 */
			String language;
		}

		public static class DeleteCrumbsParams {
			/**
			 * Required. The ID of the entity whose crumbs should be deleted.
			 */
			private String entityId;

			/**
			 * Required. A definition of a [min, max] range within which crumbs
			 * should be deleted.
			 */
			private double minTimestamp;
			/**
			 * Required. A definition of a [min, max] range within which crumbs
			 * should be deleted.
			 */
			private double maxTimestamp;

			public String getEntityId() {
				return entityId;
			}

			public void setEntityId(String entityId) {
				this.entityId = entityId;
			}

			public double getMinTimestamp() {
				return minTimestamp;
			}

			public void setMinTimestamp(double minTimestamp) {
				this.minTimestamp = minTimestamp;
			}

			public double getMaxTimestamp() {
				return maxTimestamp;
			}

			public void setMaxTimestamp(double maxTimestamp) {
				this.maxTimestamp = maxTimestamp;
			}

			public DeleteCrumbsParams(String entityId, double minTimestamp,
					double maxTimestamp) {
				this.entityId = entityId;
				this.maxTimestamp = maxTimestamp;
				this.minTimestamp = minTimestamp;
			}
		}

		private static String createCrumbsRecordRequestBody(Entity entity,
				List<Crumb> crumbs) throws IOException {
			String entityId = entity.getId();
			return createCrumbsRecordRequestBody(entityId, crumbs);
		}

		public static String createCrumbsRecordRequestBody(String entityId,
				List<Crumb> crumbs) throws IOException {
			RecordCrumbsParam params = new RecordCrumbsParam(entityId, crumbs);
			String body = Utils.createRequestBodyForObj(params);
			return body;
		}

	}

	public static class GeoFencesServices extends BaseService {

		public static final String CREATE_SVC_METHOD = "geofences/create";

		public static final String ADD_MEMBER_METHOD = "geofences/addmembers";

		public static final String REM_MEMBER_METHOD = "geofences/removemembers";

		public static final String LIST_METHOD = "geofences/list";

		public static final String DELETE_METHOD = "geofences/delete";

		public static final String ACTIVE_METHOD = "geofences/getrecentlyactive";

		public static List<GeoFence> createGeoFences(List<GeoFence> geofences)
				throws IOException, GeneralSecurityException,
				URISyntaxException {

			String requestBody = Utils
					.createRequestBodyForObj(new CreateGeofenceParam(geofences));
			InputStream result = callMethod(CREATE_SVC_METHOD, requestBody);

			List<String> list = createResultGeofencesIdsList(result);
			int i = 0;
			for (GeoFence fence : geofences) {
				fence.setId(list.get(i++));
			}
			return geofences;
		}

		private static List<String> createResultGeofencesIdsList(
				InputStream stream) throws IOException {
			Gson gson = new Gson();
			// String str = getStringFromInputStream(stream);
			// System.out.println(str);
			GeofencesParams geofencesIds = gson.fromJson(new InputStreamReader(
					stream), GeofencesParams.class);
			return geofencesIds.getGeofenceIds();
		}

		public static boolean addMemberToGeoFence(MemberOperationParam param)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String requestBody = Utils.createRequestBodyForObj(param);
			InputStream result = callMethod(ADD_MEMBER_METHOD, requestBody);
			return checkEmpty(result);
		}

		public static boolean removeMemeberFromGeoFence(
				MemberOperationParam param) throws IOException,
				GeneralSecurityException, URISyntaxException {
			String requestBody = Utils.createRequestBodyForObj(param);
			InputStream result = callMethod(REM_MEMBER_METHOD, requestBody);
			return checkEmpty(result);
		}

		public static List<GeoFence> listGeoFences(GeofencesListParams params)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String requestBody = Utils.createRequestBodyForObj(params);
			InputStream result = callMethod(LIST_METHOD, requestBody);
			List<GeoFence> geofences = createResultGeoFenceListForResult(result);
			return geofences;
		}

		/**
		 * The geofences/delete method allows you to delete a list of geofences.
		 * The geofences will no longer show up or be usable in other parts of
		 * the API.
		 * 
		 * The following properties are supported in requests:
		 * 
		 * geofenceIds: An array of IDs of geofences to delete. It is legal, but
		 * a no-op, to delete geofences that have already been deleted.
		 * 
		 * @param params
		 * @return
		 * @throws IOException
		 * @throws GeneralSecurityException
		 * @throws URISyntaxException
		 */

		public static boolean removeGeoFences(GeofencesParams params)
				throws IOException, GeneralSecurityException,
				URISyntaxException {
			String requestBody = Utils.createRequestBodyForObj(params);
			InputStream result = callMethod(DELETE_METHOD, requestBody);
			return checkEmpty(result);
		}

		/**
		 * Retrieving active geofences
		 * 
		 * When a crumb is recorded for an entity, it may activate a geofence
		 * associated with a collection that contains the entity. For example,
		 * the entity may have strayed outside of a region within which it is
		 * expected to stay.
		 * 
		 * Using the geofence/getrecentlyactive method you can retrieve a list
		 * of recent geofence events for a particular collection. The method
		 * takes a collection ID as input, and returns one result per
		 * entity/geofence pair, along with a recent crumb for the entity that
		 * made the geofence active. Results are not returned for entities and
		 * geofences that have not recently been active.
		 * 
		 * Entities that were a member of the collection at the time that they
		 * activated the geofence are considered. Note that the method may
		 * return results for entities that are no longer members of the
		 * collection, including entities that have since been deleted. Because
		 * the method returns only recent events, these entries will eventually
		 * expire and no longer be returned.
		 * 
		 * The following properties are recognized in the request:
		 * 
		 * collectionId: Required. The ID of the collection whose geofence
		 * activity is of interest. Here is an example of a request:
		 * 
		 * @param stream
		 * @return
		 * @throws IOException
		 */

		public static List<GeofenceActivityResult> recentlyActiveGeoFences(
				ActiveGeofenceParam params) throws IOException,
				GeneralSecurityException, URISyntaxException {
			String requestBody = Utils.createRequestBodyForObj(params);
			InputStream result = callMethod(ACTIVE_METHOD, requestBody);
			List<GeofenceActivityResult> geofences = createResultGeoFenceActivity(result);
			return geofences;
		}

		public static class ActiveGeofenceParam {

			private String collectionId;

			public ActiveGeofenceParam(String collectionId) {
				this.collectionId = collectionId;
			}

			public ActiveGeofenceParam(EntityCollection collection) {
				this(collection.getId());
			}

			public String getCollectionId() {
				return collectionId;
			}

			public void setCollectionId(String collectionId) {
				this.collectionId = collectionId;
			}

		}

		private static List<GeofenceActivityResult> createResultGeoFenceActivity(
				InputStream stream) throws IOException {
			Gson gson = new Gson();
			GeofenceActivityResultList geofences = gson.fromJson(
					new InputStreamReader(stream),
					GeofenceActivityResultList.class);
			return geofences.getResults();
		}

		public static class GeofenceActivityResultList {
			private List<GeofenceActivityResult> results;

			public List<GeofenceActivityResult> getResults() {
				return results;
			}

			public void setResults(List<GeofenceActivityResult> results) {
				this.results = results;
			}
		}

		private static List<GeoFence> createResultGeoFenceListForResult(
				InputStream stream) throws IOException {
			Gson gson = new Gson();
			GeofenceListResult geofences = gson.fromJson(new InputStreamReader(
					stream), GeofenceListResult.class);
			return geofences.getGeofences();
		}

		public static class GeofenceListResult {

			private List<GeoFence> geofences;

			public GeofenceListResult() {
				setGeofences(new ArrayList<GeoFence>());
			}

			public List<GeoFence> getGeofences() {
				return geofences;
			}

			public void setGeofences(List<GeoFence> geofences) {
				this.geofences = geofences;
			}
		}

		public static class GeofencesParams {

			private List<String> geofenceIds;

			public GeofencesParams() {
				setGeofenceIds(new ArrayList<String>());
			}

			public GeofencesParams(List<GeoFence> geofences) {
				List<String> ids = GeoFencesList.getGeoFenceIds(geofences);
				setGeofenceIds(ids);
			}

			public GeofencesParams(GeoFence geofences) {
				List<String> ids = new ArrayList<String>();
				ids.add(geofences.getId());
				setGeofenceIds(ids);
			}

			public List<String> getGeofenceIds() {
				return geofenceIds;
			}

			public void setGeofenceIds(List<String> geofenceIds) {
				this.geofenceIds = geofenceIds;
			}

		}

		public static class GeofencesListParams extends GeofencesParams {

			private String minId;

			public GeofencesListParams() {
				super();
				setMinId(null);
				setGeofenceIds(null);
			}

			public String getMinId() {
				return minId;
			}

			public void setMinId(String minId) {
				this.minId = minId;
			}
		}

		public static class CreateGeofenceParam {
			private List<GeoFence> geofences;

			public CreateGeofenceParam(List<GeoFence> geofences) {
				this.setGeofences(geofences);
			}

			public List<GeoFence> getGeofences() {
				return geofences;
			}

			public void setGeofences(List<GeoFence> geofences) {
				this.geofences = geofences;
			}
		}

		public static class MemberOperationParam {
			/**
			 * Required. The ID of the geofence to be modified.
			 */
			private String geofenceId;

			/**
			 * Required. An array of IDs of collections to be added to the
			 * geofence. It is legal, but a no-op to add collections that are
			 * already present in the geofence.
			 */
			private List<String> collectionIds;

			/**
			 * An array of IDs of entities to be added to the geofence. It is
			 * legal, but a no-op to add entities that are already present in
			 * the geofence. This must be specified.
			 */
			List<String> entityIds;

			public MemberOperationParam() {
				setCollectionIds(new ArrayList<String>());
				entityIds = new ArrayList<String>();
			}

			public MemberOperationParam(GeoFence geofence,
					List<String> collectionIDs, List<String> entityIds) {
				this.setCollectionIds(collectionIDs);
				this.entityIds = entityIds;
				this.setGeofenceId(geofence.getId());
			}

			public MemberOperationParam(GeoFence geofence,
					List<String> collectionIDs) {
				this(geofence, collectionIDs, null);
			}

			public String getGeofenceId() {
				return geofenceId;
			}

			public void setGeofenceId(String geofenceId) {
				this.geofenceId = geofenceId;
			}

			public List<String> getCollectionIds() {
				return collectionIds;
			}

			public void setCollectionIds(List<String> collectionIds) {
				this.collectionIds = collectionIds;
			}
		}

	}
}
