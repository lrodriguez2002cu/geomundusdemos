package es.uji.geotec.geomundus.trackclient;

/**
 * "crumb": { "confidenceRadius": 3.14, "location": { "lat": 43.0, "lng": -108.0
 * }, "timestamp": 1341375000.0, "userData": { "driver_name": "Joe",
 * "measured_vehicle_speed": "110.2" } }, "entityId": "1ff3a55f94e954ee",
 * "geofenceId": "7c5e3c8f37d11bfc"
 * 
 * @author Luis
 * 
 */
public class GeofenceActivityResult {
	
	private Crumb crumb;
	private String entityId;
	private String geofenceId;

	public Crumb getCrumb() {
		return crumb;
	}

	public void setCrumb(Crumb crumb) {
		this.crumb = crumb;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getGeofenceId() {
		return geofenceId;
	}

	public void setGeofenceId(String geofenceId) {
		this.geofenceId = geofenceId;
	}
	
	@Override
	public String toString() {
		return String.format("entity: %s, geofence:%s, crumb: %s", entityId, geofenceId, crumb);
	}
}
