

package es.uji.geotec.geomundus.db;

import java.sql.*;
import java.util.*;

import es.uji.geotec.geomundus.db.PropertyManager;


public class PoolManager
{

   static private PoolManager instance;
   static private int clients;

   private Vector<Driver> drivers = new Vector<Driver>();
   private Hashtable<String,ConnectionPool> pools = new Hashtable<String,ConnectionPool>();

   //private Logger log = LogManager.getLogManager().getLogger(PoolManager.class.getName());  

   private PoolManager()
   {
      init();
   }


   static synchronized public PoolManager getInstance()
   {
      if (instance == null)
      {
         instance = new PoolManager();
      }
      clients++;
      return instance;
   }

   private void init()
   {
      Properties dbProps = new Properties();
      try
      {
        dbProps = PropertyManager.getProperties(PropertyManager.DB_PROPERTIES);
      }
      catch (Exception e)
      {
    	  //log.error("Can't read the properties file. " + "Make sure db.properties is in the CLASSPATH");
         return;
      }      
      loadDrivers(dbProps);
      createPools(dbProps);
   }

   private void loadDrivers(Properties props)
   {
      String driverClasses = props.getProperty("drivers");
      StringTokenizer st = new StringTokenizer(driverClasses);
      while (st.hasMoreElements())
      {
         String driverClassName = st.nextToken().trim();
         try
         {
            Driver driver = (Driver)
               Class.forName(driverClassName).newInstance();
            DriverManager.registerDriver(driver);
            drivers.addElement(driver);
            //log.debug("Registered JDBC driver " + driverClassName);              
         }
         catch (Exception e)
         {
        	 //log.error("Can't register JDBC driver: " +driverClassName);
         }
      }
   }

   private void createPools(Properties props)
   {
	   /*
      Enumeration<String> propNames = (Enumeration<String>) props.propertyNames();
      while (propNames.hasMoreElements())
      {
         String name = (String) propNames.nextElement();
         if (name.endsWith(".url")){
            String poolName = name.substring(0, name.lastIndexOf("."));
            String url = props.getProperty(poolName + ".url");
            if (url == null){
               log.debug("No URL specified for " + poolName);
               continue;
            }

            String user = props.getProperty(poolName + ".user");
            String password = props.getProperty(poolName + ".password");

            String maxConns = props.getProperty(poolName +
               ".maxconns", "0");
            int max;
            try
            {
               max = Integer.valueOf(maxConns).intValue();
            }
            catch (NumberFormatException e)
            {
            	//log.error("Invalid maxconns value " + maxConns +" for " + poolName);
               max = 0;
            }

            String initConns = props.getProperty(poolName +
                              ".initconns", "0");
            int init;
            try
            {
               init = Integer.valueOf(initConns).intValue();
            }
            catch (NumberFormatException e)
            {
            	// log.error("Invalid initconns value " + initConns +" for " + poolName);
               init = 0;
            }

            String loginTimeOut = props.getProperty(poolName +
               ".logintimeout", "5");
            int timeOut;
            try
            {
               timeOut = Integer.valueOf(loginTimeOut).intValue();
            }
            catch (NumberFormatException e)
            {
            	//log.error("Invalid logintimeout value " + loginTimeOut +
                            " for " + poolName);
               timeOut = 5;
            }

            String logLevelProp = props.getProperty(poolName +
                           ".loglevel", String.valueOf(LogWriter.ERROR));
            int logLevel = LogWriter.INFO;
            if (logLevelProp.equalsIgnoreCase("none"))
            {
               logLevel = LogWriter.NONE;
            }
            else if (logLevelProp.equalsIgnoreCase("error"))
            {
               logLevel = LogWriter.ERROR;
            }
            else if (logLevelProp.equalsIgnoreCase("debug"))
            {
               logLevel = LogWriter.DEBUG;
            }
            
            ConnectionPool pool = new ConnectionPool(poolName, url, user, password,
                                            max, init, timeOut);
            pools.put(poolName, pool);
        }
      }
      */
   }

   /** returns a connection from the pool named: 'default'*/
   public Connection getConnection()
   {
      Connection conn = null;
      ConnectionPool pool = (ConnectionPool) pools.get("default");
      if (pool != null)
      {
         try
         {
            conn = pool.getConnection();
         }
         catch (SQLException e)
         {
        	 //log.error("Exception getting pool connection");
        	 e.printStackTrace();
         }
      }
      return conn;
   }

   public Connection getConnection(String name)
   {
      Connection conn = null;
      ConnectionPool pool = (ConnectionPool) pools.get(name);
      if (pool != null)
      {
         try
         {
            conn = pool.getConnection();
         }
         catch (SQLException e)
         {
        	 //log.error("Exception getting pool connection");
        	 e.printStackTrace();
         }
      }
      return conn;
   }

   /** Free a connection to the default database as specified in the
    *  properties file: db.properties
    */
   public void freeConnection(Connection con)
   {
      ConnectionPool pool = (ConnectionPool) pools.get("default");
      if (pool != null)
      {
         pool.freeConnection(con);
      }
   }

   public void freeConnection(String name, Connection con)
   {
      ConnectionPool pool = (ConnectionPool) pools.get(name);
      if (pool != null)
      {
         pool.freeConnection(con);
      }
   }

   public synchronized void release()
   {
      // Wait until called by the last client
      if (--clients != 0)
      {
         return;
      }
      Enumeration allPools = pools.elements();
      while (allPools.hasMoreElements())
      {
         ConnectionPool pool = (ConnectionPool) allPools.nextElement();
         pool.release();
      }
      Enumeration allDrivers = drivers.elements();
      while (allDrivers.hasMoreElements())
      {
         Driver driver = (Driver) allDrivers.nextElement();
         try
         {
            DriverManager.deregisterDriver(driver);
            //log.debug("Deregistered JDBC driver " +driver.getClass().getName());
         }
         catch (SQLException e)
         {
        	 //log.error("Couldn't deregister JDBC driver: " + driver.getClass().getName());
        	 e.printStackTrace();
         }
      }
   }
}