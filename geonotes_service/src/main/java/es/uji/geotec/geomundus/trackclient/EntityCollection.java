package es.uji.geotec.geomundus.trackclient;

import java.util.ArrayList;
import java.util.List;

/**
 * A collection is any number of entities grouped together for the purpose of
 * tracking in the API. Collections can be associated with one or more
 * geofences, so that all entities in the collection trigger the same geofences;
 * recent crumbs can also be returned for an entire collection, so that the
 * recent location of all entities in the collection can be plotted.
 * 
 * Each collection has a number of properties:
 * 
 * ID: An opaque string used to refer to the collection in calls to various
 * methods. The ID for a collection is assigned by the API at creation time.
 * There are no guarantees about its form except that it will be unique across
 * collections for a particular Service Account client ID.
 * 
 * Name: A user-defined string describing the collection. You may use this for
 * any purpose you like; for example, recording a human-readable name to show to
 * your application's end users. Names must consist of Unicode text and need not
 * be unique. The name for a collection may be left unspecified.
 * 
 * Entity IDs: Zero or more IDs of entities that belong to the collection.
 * Initially the collection is empty, but you can make API calls to add and
 * remove entities.
 * 
 * @author Luis
 * 
 */
public class EntityCollection {

	private String id;
	private String name;
	private List<String> entityIds;

	public EntityCollection() {
		setEntityIds(new ArrayList<String>());
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		
		return String.format("Collection (name: %s, id: %s, entities: %s )", name, id, getEntityIds());
	}
	
	public List<String> getEntityIds() {
		return entityIds;
	}

	public void setEntityIds(List<String> entityIds) {
		this.entityIds = entityIds;
	}

	private static class CollectionName  {
		String name;
		
		public CollectionName(String name) {
			this.name = name;
		}
	}
	
	public static class CollectionNames{
		private List<CollectionName> collections;
		
		public CollectionNames(List<EntityCollection> collections) {
			this.setCollections(new ArrayList<EntityCollection.CollectionName>());
			for (EntityCollection col : collections){
				this.getCollections().add(new CollectionName(col.getName()));
			}
		}

		public List<CollectionName> getCollections() {
			return collections;
		}

		public void setCollections(List<CollectionName> collections) {
			this.collections = collections;
		}
	}

	public static class CollectionList {
		
		private List<EntityCollection> collections;
		
		public CollectionList() {
			setCollections(new ArrayList<EntityCollection>());
		}

		public List<EntityCollection> getCollections() {
			return collections;
		}

		public void setCollections(List<EntityCollection> collections) {
			this.collections = collections;
		}
		
		public static List<String> getCollectionsIds(List<EntityCollection> collections){
			List <String> ids = new ArrayList<String>();
			for (EntityCollection col : collections){
				ids.add(col.getId());
			}
			return ids;
		}
		
		public static List<String> getCollectionsIds(EntityCollection collection){
			List <String> ids = new ArrayList<String>();
			ids.add(collection.getId());
			return ids;
		}
		
		public static EntityCollection getCollectionByName(List<EntityCollection> collections, String name){
			for (EntityCollection col : collections){
				if (col.getName().equalsIgnoreCase(name)){
					return col;
				}
			}
			return null;
		}
	}
	
}
