package es.uji.geotec.geomundus.trackclient;

import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import es.uji.geotec.geomundus.common.LatLon;

/**
 * Crumbs
 * 
 * Overview Recording crumbs Getting recent crumbs Retrieving historical crumbs
 * Summarizing an entity's path Retrieving information about an entity's
 * location Deleting crumbs Overview
 * 
 * Developers � Google Maps API � Web iOS Android Showcase Support Blog GDG Live
 * A crumb is a single data point stored for an entity. It consists of a
 * geographical position, a timestamp, and user-defined key-value string pairs.
 * The latter allows the user to store their own data for later retrieval; for
 * example, information about a delivery vehicle's current contents, or a rental
 * car's current renter.
 * 
 * Crumbs contain the following properties:
 * 
 * location: An object consisting of numeric lat and lng properties. This must
 * be specified when recording crumbs.
 * 
 * timestamp: The time at which the location was measured. This must be
 * specified when recording crumbs. Timestamps must be expressed as seconds
 * since 1970-01-01 00:00:00 UTC, i.e. POSIX time. Timestamps are expressed as
 * floating point numbers, so they may represent fractional seconds.
 * 
 * confidenceRadius: An inversely-correlated measure of the precision of the
 * location measurement. This should be set to the radius in meters of a 95%
 * confidence interval around the location point if available, or omitted
 * otherwise. If present, it must be positive and no more than 35 km.
 * 
 * heading: The entity's heading in degrees. Headings are expressed in degrees
 * clockwise from north within the range [0, 360). This value should be omitted
 * if it is unknown.
 * 
 * userData: An object containing arbitrary string keys and string values, for
 * use by the user in an application-specific manner. This may be omitted.
 * 
 * Below is an example crumb:
 * 
 * @author lrodriguez
 * 
 */
public class Crumb {

	/**
	 * An object consisting of numeric lat and lng properties. This must be
	 * specified when recording crumbs.
	 */
	private LatLon location;

	/**
	 * The time at which the location was measured. This must be specified when
	 * recording crumbs. Timestamps must be expressed as seconds since
	 * 1970-01-01 00:00:00 UTC, i.e. POSIX time. Timestamps are expressed as
	 * floating point numbers, so they may represent fractional seconds.
	 */

	private double timestamp;

	/**
	 * An inversely-correlated measure of the precision of the location
	 * measurement. This should be set to the radius in meters of a 95%
	 * confidence interval around the location point if available, or omitted
	 * otherwise. If present, it must be positive and no more than 35 km.
	 */
	private double confidenceRadius;

	/**
	 * The entity's heading in degrees. Headings are expressed in degrees
	 * clockwise from north within the range [0, 360). This value should be
	 * omitted if it is unknown.
	 */
	private double heading;

	/**
	 * : An object containing arbitrary string keys and string values, for use
	 * by the user in an application-specific manner. This may be omitted.
	 */
	private Map<String, String> userData;

	public LatLon getLocation() {
		return location;
	}

	public void setLocation(LatLon location) {
		this.location = location;
	}

	public double getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(double timestamp) {
		this.timestamp = timestamp;
	}

	public double getConfidenceRadius() {
		return confidenceRadius;
	}

	public void setConfidenceRadius(double confidenceRadius) {
		this.confidenceRadius = confidenceRadius;
	}

	public double getHeading() {
		return heading;
	}

	public void setHeading(double heading) {
		this.heading = heading;
	}

	public Map<String, String> getUserData() {
		return userData;
	}

	public void setUserData(Map<String, String> userData) {
		this.userData = userData;
	}
	
	public Crumb() {
		
	}
	
	public Crumb(LatLon latlong, double secs, int radius, double heading){
		this.location = latlong;
		this.timestamp = secs;
		this.confidenceRadius = radius;
		this.heading = heading;
	}

	public Crumb(LatLon latlong, DateTime time, int radius, double heading){
		this(latlong, getSecs(time), radius, heading);
	}
	
	@Override
	public String toString() {
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		long time = (long) timestamp* 1000;
		String fmtStr = fmt.print(time); 
		return String.format("Crumb(location: %s, timestamp: %s)", location.toString(), fmtStr);
	}
	
	public DateTime getTime(){
		long longtime = (long) timestamp* 1000;
		DateTime time = new DateTime(longtime);
		return time;
	}
	
	private static double getSecs(DateTime datetime){
		return datetime.toInstant().getMillis() / 1000;
	}
}
