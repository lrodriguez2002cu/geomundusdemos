package es.uji.geotec.geomundus.trackclient;

import java.util.ArrayList;
import java.util.List;

public class Polyline {
	
	private List<Vertex> vertices;

	public Polyline() {
		setVertices(new ArrayList<Vertex>());
	}

	public List<Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(List<Vertex> vertices) {
		this.vertices = vertices;
	}
	
	@Override
	public String toString() {
		return String.format("Polyline(vetices: %s)", vertices);
	}
}
