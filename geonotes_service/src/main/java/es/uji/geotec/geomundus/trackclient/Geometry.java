package es.uji.geotec.geomundus.trackclient;

import java.util.ArrayList;
import java.util.List;

/**
 * Polygons - which in the Google Maps Tracks API are associated with geofences
 * - define geographic regions, and are composed of one or more loops, with no
 * two loops intersecting each other. A loop is a list of vertices, defining a
 * simple region on the sphere. Normally a point is inside a polygon if it is
 * contained by an odd number of loops. Polygons can be inverted so that they
 * contain the complement of what the polygon would otherwise contain
 * 
 * @author Luis
 * 
 */
public class Geometry {

	private boolean invert;
	private List<Loop> loops;

	public boolean isInvert() {
		return invert;
	}

	public void setInvert(boolean invert) {
		this.invert = invert;
	}

	public List<Loop> getLoops() {
		return loops;
	}

	public void setLoops(List<Loop> loops) {
		this.loops = loops;
	}
	
	public Geometry() {
		 loops = new ArrayList<Loop>();
	}
	
	public Geometry(List<Loop> loops) {
		 this.loops = loops;
	}
	
	public Geometry(List<Loop> loops, boolean invert) {
		 this.loops = loops;
		 this.invert = invert;
	}
	
	@Override
	public String toString() {
		return String.format("Geometry (invert: %s, loops: %s)", Boolean.toString(invert), loops.toString());
	}
}
