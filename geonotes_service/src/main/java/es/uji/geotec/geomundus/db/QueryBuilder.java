package es.uji.geotec.geomundus.db;

import java.util.List;

import es.uji.geotec.geomundus.common.*;

/**
 * Class in charge of building queries to retrieve information from the note
 * database
 * 
 * @author lrodriguez
 * 
 */
public class QueryBuilder {

	public final static String SCHEMA_NAME = "public";

	public final static class ENTRIES_TABLE {

		public static String TABLE_NAME = "note_entries";
		public static String TEXT_COL = "text";
		public static String USER_COL = "user";
		public static String NOTE_COL = "noteid";
		public static String ID_COL = "note_entry_id";

	}

	public final static class NOTES_TABLE {
		public static String TABLE_NAME = "notes";
		public static String LAT_COL = "latitude";
		public static String LON_COL = "longitude";
		public static String NOTE_COL = "noteid";
	}


	public static String getNotesList() {
		String query = "SELECT * FROM " + SCHEMA_NAME + "."
				+ NOTES_TABLE.TABLE_NAME;
		return query;
	}

	public static String getNotesList(int radius, LatLon location) {
		String query = String.format("SELECT * FROM "+ NOTES_TABLE.TABLE_NAME + " WHERE ST_DISTANCE (st_Point(latitude, longitude)" +
				" :: geography, st_POINT(%s, %s):: geography) <= %d", location.getLat(), location.getLng(), radius);
		return query;
	}
	public static String getNoteById(String noteId) {
		String query = String.format("SELECT * FROM " + SCHEMA_NAME + "."
				+ NOTES_TABLE.TABLE_NAME + " WHERE " + NOTES_TABLE.NOTE_COL + " = '%s'", noteId);
		return query;
	}

	
	public static String getNotesEntriesForNote(String noteid) {
		return String.format("SELECT * " + " FROM "
				+ ENTRIES_TABLE.TABLE_NAME + " WHERE " + ENTRIES_TABLE.NOTE_COL
				+ " = '%1$s' ", noteid);
	}

	public static String insertNoteStatement(String id, double latitude, double longitude){
		return String.format("INSERT INTO notes( " +
            " noteid, latitude, longitude) " +
            "VALUES ('%s', %s, %s)", id , Double.toString(latitude),  Double.toString(longitude));
	}
	
	public static String insertNoteEntryStatement(String noteid, String text, String user){
		return String.format("INSERT INTO note_entries( " +
            " noteid, \"user\", text) " +
            " VALUES ( '%s', '%s', '%s') " , noteid , user, text);
	}
	
	public static String deleteNoteStatement(String noteid){
		return String.format("delete from notes where noteid='%s'", noteid );
	}

	public static String insertUser(User user) {
		return String.format("INSERT INTO users( " +
	            " id, name) " +
	            " VALUES ( '%s', '%s') " , user.getId() , user.getName());
	}
	
	public static String getUsers() {
		return "select * from users";
	}

}
