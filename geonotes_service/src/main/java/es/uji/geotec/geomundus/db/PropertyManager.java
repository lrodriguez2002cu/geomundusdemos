
package es.uji.geotec.geomundus.db;

import java.util.Properties;
import java.util.Hashtable;
import java.io.InputStream;
import java.io.IOException;

/**
  * Description: Loads the configuration files
  */
public class PropertyManager
{
  /** the properties configuration file **/
  public final static String DB_PROPERTIES = "uji/geotec/geomundus/conf/connection.properties";
  
  //The propertyfilenames to load
  private final static String[] s_propertyFileNames = new String[]{DB_PROPERTIES};

  //Hashtable containing all propertiesobjects that are loaded
  private static Hashtable<String, Properties> s_propertyFiles = null;

  /**
  * Gets a properties object
  * If the the propertyfiles are not yet loaded, then loads them first
  *
  * @param propertyFileName, String
  * @return Properties
  * @throws java.io.IOException
  */
  public static Properties getProperties(String propertyFileName) throws IOException
  {
    if (s_propertyFiles == null)
    {
      loadProperties();
    }

    return (Properties)s_propertyFiles.get(propertyFileName);
  }

  //Loads the propertyfiles
  private static synchronized void loadProperties() throws IOException
  {
    s_propertyFiles = new Hashtable<String, Properties>(s_propertyFileNames.length);
    ClassLoader loader = PropertyManager.class.getClassLoader();

    for (int i = 0; i < s_propertyFileNames.length; i++)
    {
      InputStream input = loader.getResourceAsStream(s_propertyFileNames[i]);
      Properties props = new Properties();
      props.load(input);
      s_propertyFiles.put(s_propertyFileNames[i], props);
    }

  }

 }