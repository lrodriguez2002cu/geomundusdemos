package es.uji.geotec.geomundus.trackclient;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import es.uji.geotec.geomundus.common.LatLon;

/**
 * { "location": { "lat": -33.866495, "lng": 151.195446 }, "timestamp":
 * 1.34137506219E9 },
 * 
 * @author Luis
 * 
 */
public class Vertex {
	
	private LatLon location;
	private long timestamp;

	public LatLon getLocation() {
		return location;
	}

	public void setLocation(LatLon location) {
		this.location = location;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	@Override
	public String toString() {
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		long time = (long) timestamp * 1000;
		String fmtStr = fmt.print(time); 
		return String.format("Vertex (location: %s, timestamp: %s)", location.toString(), fmtStr);
	}
	
	
}
