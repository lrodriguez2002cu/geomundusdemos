package es.uji.geotec.geomundus.trackclient;

import java.util.ArrayList;
import java.util.List;

import es.uji.geotec.geomundus.common.LatLon;

public class Loop {
	
	private List<LatLon> vertices;

	public List<LatLon> getVertices() {
		return vertices;
	}

	public void setVertices(List<LatLon> vertices) {
		this.vertices = vertices;
	}
	
	public Loop() {
		vertices = new ArrayList<LatLon>();
	}

	@Override
	public String toString() {
		return String.format("%s", vertices) ;
	}
}
