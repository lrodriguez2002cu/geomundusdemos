package es.uji.geotec.geomundus.trackclient;

import java.util.ArrayList;
import java.util.List;

public class Entity {
	/**
	 * A user-defined name for the entity.
	 */
	private String name;

	/**
	 * (Optional) The type of the entity expressed as one of the following
	 * strings: AUTOMOBILE: A car or passenger vehicle. TRUCK: A truck or cargo
	 * vehicle. WATERCRAFT: A boat or other waterborne vehicle. PERSON: A
	 * person.
	 */
	private EntityType type;

	private String id;

	public static enum EntityType {
		AUTOMOBILE, TRUCK, WATERCRAFT, PERSON
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EntityType getType() {
		return type;
	}

	public void setType(EntityType type) {
		this.type = type;
	};

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("Entity(id: %s, name: %s, type: %s)", id, name, type.name());
	}
	
	public static class EntityIds {
		private List<String> entityIds;

		public EntityIds(List<Entity> entities) {
			this.setEntityIds(EntityList.getEntitiesIds(entities));
		}

		public List<String> getEntityIds() {
			return entityIds;
		}

		public void setEntityIds(List<String> entityIds) {
			this.entityIds = entityIds;
		}
	}

	public static class EntityList {

		private List<Entity> entities;

		public EntityList() {
			entities = new ArrayList<Entity>();
		}

		public EntityList(List<Entity> entities) {
			this.entities = entities;
		}

		public List<Entity> getEntities() {
			return entities;
		}

		public void setEntities(List<Entity> entities) {
			this.entities = entities;
		}

		public List<String> getIds() {
			List<String> ids = EntityList.getEntitiesIds(entities);
			return ids;
		}

		public static List<String> getEntitiesIds(List<Entity> entities) {
			List<String> ids = new ArrayList<String>();
			for (Entity ent : entities) {
				ids.add(ent.getId());
			}
			return ids;
		}
	}

}
