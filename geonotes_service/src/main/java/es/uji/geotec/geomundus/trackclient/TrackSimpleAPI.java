package es.uji.geotec.geomundus.trackclient;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import es.uji.geotec.geomundus.common.LatLon;
import es.uji.geotec.geomundus.trackclient.Entity.EntityType;
import es.uji.geotec.geomundus.trackclient.EntityCollection.CollectionList;
import es.uji.geotec.geomundus.trackclient.TracksClient.GeoFencesServices.ActiveGeofenceParam;
import es.uji.geotec.geomundus.trackclient.TracksClient.GeoFencesServices.GeofencesListParams;
import es.uji.geotec.geomundus.trackclient.TracksClient.GeoFencesServices.MemberOperationParam;
import es.uji.geotec.geomundus.trackclient.*;

public class TrackSimpleAPI {

	public static boolean deleteCollection(EntityCollection coll)
			throws IOException, GeneralSecurityException, URISyntaxException {
		List<EntityCollection> deleteCollection = new ArrayList<EntityCollection>();
		deleteCollection.add(coll);
		// if (
		return TracksClient.CollectionsServices
				.deleteCollections(deleteCollection);// ) {
		// List<EntityCollection> collections =
		// TrackSimpleAPI.getAllCollections();
		// System.out.println(String.format("deleted: %s, collections: %s",
		// coll.getId(), collections));

		// } else {
		// System.out.println("Collecition not deleted");
		// }
	}

	public static List<GeoFence> createGeofences(List<GeoFence> geofences)
			throws IOException, GeneralSecurityException, URISyntaxException {
		List<GeoFence> result = TracksClient.GeoFencesServices
				.createGeoFences(geofences);
		System.out.println(String.format("Geofences created: %s", result));
		return result;
	}

	public static GeoFence createGeofencesAround(LatLon position, int radius,
			String name) throws IOException, GeneralSecurityException,
			URISyntaxException {

		GeoFence geofence = new GeoFence(name, getFenceForPoint(position));
		List<GeoFence> geofences = new ArrayList<GeoFence>();
		geofences.add(geofence);
		List<GeoFence> result = TracksClient.GeoFencesServices
				.createGeoFences(geofences);
		System.out.println(String.format("Geofences created: %s", result));
		return result.get(0);
	}

	public static List<GeoFence> getGeofences() throws IOException, GeneralSecurityException,
			URISyntaxException {
		List<GeoFence> result = null;
		result = TracksClient.GeoFencesServices
				.listGeoFences(new GeofencesListParams());
		System.out.println(String.format("Geofences listed: %s", result));
		return result;
	}

	private static Geometry getFenceForPoint(LatLon position) {
		double delta = 0.0001;
		LatLon topLeft = new LatLon(position.getLat() + delta,
				position.getLng() - delta);
		LatLon topRigt = new LatLon(position.getLat() + delta,
				position.getLng() + delta);
		LatLon bottomRight = new LatLon(position.getLat() - delta,
				position.getLng() + delta);
		LatLon bottomLeft = new LatLon(position.getLat() - delta,
				position.getLng() - delta);
		List<LatLon> latLons = new ArrayList<LatLon>();
		latLons.add(topLeft);
		latLons.add(topRigt);
		latLons.add(bottomRight);
		latLons.add(bottomLeft);
		Geometry geom = getGeometry(latLons);
		return geom;

	}

	public static Geometry getGeometry(List<LatLon> cafeteria) {
		Geometry geo = new Geometry();
		List<Loop> loops = new ArrayList<Loop>();
		Loop loop = new Loop();
		loop.setVertices(cafeteria);
		loops.add(loop);
		geo.setLoops(loops);
		return geo;
	}

	public static Entity createEntity(String entityName) throws IOException,
			GeneralSecurityException, URISyntaxException {
		Entity entity = new Entity();
		entity.setName(entityName);
		entity.setType(EntityType.PERSON);
		List<Entity> entities = new ArrayList<Entity>();
		entities.add(entity);
		entities = TracksClient.EntitiesServices.createEntity(entities);
		System.out.println(entities.toString());
		return entities.get(0);
	}

	public static EntityCollection createCollection(String collName)
			throws IOException, GeneralSecurityException, URISyntaxException {
		EntityCollection collection = new EntityCollection();
		collection.setName(collName);
		List<EntityCollection> collections = new ArrayList<EntityCollection>();
		collections.add(collection);
		collections = TracksClient.CollectionsServices
				.createCollections(collections);
		System.out.println(collections);
		return collections.get(0);
	}

	public static boolean addCollectionToGeoFence(
			EntityCollection peopleCollection, GeoFence geofence)
			throws IOException, GeneralSecurityException, URISyntaxException {
		MemberOperationParam addMemeberParam = new MemberOperationParam(
				geofence, CollectionList.getCollectionsIds(peopleCollection));

		boolean memberAdded = TracksClient.GeoFencesServices
				.addMemberToGeoFence(addMemeberParam);

		System.out.println(String.format("Geofence member added: %s",
				Boolean.toString(memberAdded)));
		return memberAdded;
	}

	public static List<Entity> getEntities() throws IOException,
			GeneralSecurityException, URISyntaxException {
		List<Entity> entities = TracksClient.EntitiesServices.getAllEntities();
		System.out.println(entities.toString());
		return entities;
	}

	public static boolean deleteAllCollections() throws IOException,
			GeneralSecurityException, URISyntaxException {
		List<EntityCollection> deleteCollections = TrackSimpleAPI
				.getAllCollections();
		boolean result = TracksClient.CollectionsServices
				.deleteCollections(deleteCollections);
		if (result) {
			List<EntityCollection> collections = TrackSimpleAPI
					.getAllCollections();
			System.out.println(String.format("deleted all collections: %s",
					collections));
		} else {
			System.out.println("Collecition not deleted");
		}
		return result;
	}

	public static EntityCollection addEntityToCollection(Entity entity,
			EntityCollection collection) throws IOException,
			GeneralSecurityException, URISyntaxException {
		List<Entity> entities = new ArrayList<Entity>();
		entities.add(entity);
		return addEntitiesToCollection(entities, collection);
	}

	public static EntityCollection addEntitiesToCollection(
			List<Entity> entities, EntityCollection collection)
			throws IOException, GeneralSecurityException, URISyntaxException {
		collection = TracksClient.CollectionsServices.addEntityToCollection(
				collection, entities);
		System.out.println("Collection with entity: " + collection);
		return collection;
	}

	public static boolean recordCrumbs(Entity entity, List<Crumb> crumbs)
			throws IOException, GeneralSecurityException, URISyntaxException {
		String entityId = entity.getId();
		boolean added = recordCrumbs(entityId, crumbs);
		return added;
	}

	public static List<GeofenceActivityResult> getActiveGeofences(
			EntityCollection peopleCollection) throws IOException,
			GeneralSecurityException, URISyntaxException {
		ActiveGeofenceParam param = new ActiveGeofenceParam(
				peopleCollection.getId());

		// the geofence should be active
		List<GeofenceActivityResult> activeGeofences = TracksClient.GeoFencesServices
				.recentlyActiveGeoFences(param);
		return activeGeofences;
	}

	public static boolean recordCrumbs(String entityId, List<Crumb> crumbs)
			throws IOException, GeneralSecurityException, URISyntaxException {
		boolean added = TracksClient.CrumbsServices.recordCrumbs(entityId,
				crumbs);
		if (added) {
			System.out.println(String.format("addedCrums for entity %s ",
					entityId));
		}
		return added;
	}

	public static List<EntityCollection> getAllCollections()
			throws IOException, GeneralSecurityException, URISyntaxException {
		return TracksClient.CollectionsServices.listCollections();
	}

	public static EntityCollection getCollectionByName(String collName)
			throws IOException, GeneralSecurityException, URISyntaxException {
		List<EntityCollection> collections = TrackSimpleAPI.getAllCollections();
		EntityCollection coll = CollectionList.getCollectionByName(collections,
				collName);
		return coll;
	}

	public static boolean deleteEntities(List<Entity> entities)
			throws IOException, GeneralSecurityException, URISyntaxException {
		boolean deleted = TracksClient.EntitiesServices
				.deleteEntities(entities);
		System.out.println(String.format("Deleted entities: %s",
				Boolean.toString(deleted)));
		return deleted;
	}

	public static boolean deleteEntity(Entity entity) throws IOException,
			GeneralSecurityException, URISyntaxException {
		List<Entity> entities = new ArrayList<Entity>();
		entities.add(entity);
		return deleteEntities(entities);
	}

}