package es.uji.geomundus.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Application;

import es.uji.geotec.geomundus.common.*;

@Path("/svc")
public class NotesServices extends Application implements
		NotesServicesInterface {

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.uji.geomundus.services.NotesServicesInterface#getNotes(int,
	 * java.lang.String)
	 */
	@Override
	@GET
	@Path("/notes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Note> getNotes(@QueryParam("radius") int radius,
			@QueryParam("location") String location) {
		List<Note> notes = new ArrayList<Note>();
		Note sampleNote = getASampleNote(0);
		notes.add(sampleNote);
		sampleNote = getASampleNote(2);
		notes.add(sampleNote);
		return notes;
	}

	protected Note getASampleNote(int i) {
		Note sampleNote = new Note();
		LatLon location = new LatLon();
		sampleNote.setLocation(location);
		sampleNote.getEntries().add(
				new NoteEntry("user" + i, "Hello, this is my first note."));
		sampleNote.getEntries()
				.add(new NoteEntry("user" + (i + 1),
						"Hello, this is my first note."));
		return sampleNote;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.uji.geomundus.services.NotesServicesInterface#addNoteEntry(java.lang
	 * .String, es.uji.geomundus.common.NoteEntry)
	 */
	@Override
	@POST
	@Path("/notes/addentry/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Note addNoteEntry(@PathParam(value = "id") String noteId,
			NoteEntry noteEntry) {
		Note note = new Note();
		note.setId(noteId);
		note.getEntries().add(noteEntry);
		return note;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.uji.geomundus.services.NotesServicesInterface#addNote(es.uji.geomundus
	 * .common.LatLon)
	 */
	@Override
	@POST
	@Path("/notes/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Note addNote(LatLon location) {
		Note note = new Note();
		note.setLocation(location);
		note.setId("N_"
				+ Long.toString(Calendar.getInstance().getTimeInMillis()));
		return note;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.uji.geomundus.services.NotesServicesInterface#removeNote(java.lang
	 * .String)
	 */
	@Override
	@DELETE
	@Path("/notes/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Note removeNote(@PathParam("id") String noteId) {
		System.out.println("Removing note " + noteId);
		return new Note(noteId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.uji.geomundus.services.NotesServicesInterface#updateUserPosition(java
	 * .lang.String, es.uji.geomundus.common.LatLon)
	 */
	@Override
	@POST
	@Path("/user/position/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Note> updateUserPosition(@PathParam("id") String userId,
			LatLon userPosition) {
		System.out.println("Adding user position " + userPosition);
		return new ArrayList<Note>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.uji.geomundus.services.NotesServicesInterface#addUser(es.uji.geomundus
	 * .common.User)
	 */
	@Override
	@POST
	@Path("/adduser/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public User addUser(User user) {
		user.setId(Long.toString(Calendar.getInstance().getTimeInMillis()));
		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.uji.geomundus.services.NotesServicesInterface#getUsers()
	 */
	@Override
	@GET
	@Path("/users")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getUsers() {
		List<User> users = new ArrayList<User>();
		User user = getASampleUser("Luis");
		users.add(user);
		user = getASampleUser("Luis1");
		users.add(user);
		return users;
	}

	private User getASampleUser(String name) {
		User u = new User(name);
		u.setName(name + "_"
				+ Long.toString(Calendar.getInstance().getTimeInMillis()));
		return u;
	}

	private static List<LatLon> getCafeteriaVertices() {
		List<LatLon> list = new ArrayList<LatLon>();
		list.add(new LatLon(39.99430091179494, -0.06774723529815674));
		list.add(new LatLon(39.994524891472054, -0.06825685501098633));
		list.add(new LatLon(39.99422488182928, -0.06856530904769897));
		list.add(new LatLon(39.99397007808405, -0.06793230772018433));
		//list.add(new LatLon(39.99430091179494, -0.06774723529815674));
		return list;
	}

	@GET
	@Path("/notesfences")
	@Produces(MediaType.APPLICATION_JSON)
	public List<NotesFence> getNotesFences() {
		List<NotesFence> notesf = new ArrayList<NotesFence>();
		NotesFence fence = new NotesFence();
		fence.setName("Cafeteria");
		fence.setGeom(getCafeteriaVertices());
		return notesf;
	}

}
