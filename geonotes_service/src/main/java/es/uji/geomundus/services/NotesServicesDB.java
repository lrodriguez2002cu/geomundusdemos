package es.uji.geomundus.services;

//import java.io.IOException;
//import java.net.URISyntaxException;
//import java.security.GeneralSecurityException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
//import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.joda.time.DateTime;

import es.uji.geotec.geomundus.common.*;
import es.uji.geotec.geomundus.db.ConnectionManager;
import es.uji.geotec.geomundus.db.QueryBuilder;
//import es.uji.geotec.geomundus.trackclient.Crumb;
//import es.uji.geotec.geomundus.trackclient.Entity;
//import es.uji.geotec.geomundus.trackclient.EntityCollection;
//import es.uji.geotec.geomundus.trackclient.GeoFence;
//import es.uji.geotec.geomundus.trackclient.GeofenceActivityResult;
//import es.uji.geotec.geomundus.trackclient.Geometry;
//import es.uji.geotec.geomundus.trackclient.TrackSimpleAPI;

@Path("/svc1")
public class NotesServicesDB extends NotesServices {

	@GET
	@Path("/notes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Note> getNotes(@QueryParam("radius") int radius,
			@QueryParam("location") String location) {
		List<Note> notes = new ArrayList<Note>();
		ConnectionManager connectionManager = new ConnectionManager();
		Connection con = connectionManager.getConnection();
		String query = "";

		query = getNotesQuery(radius, location);

		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				Note note = new Note();
				note.setId(rs.getString(QueryBuilder.NOTES_TABLE.NOTE_COL));
				double lat = rs.getDouble(QueryBuilder.NOTES_TABLE.LAT_COL);
				double lon = rs.getDouble(QueryBuilder.NOTES_TABLE.LON_COL);
				note.setLocation(new LatLon(lat, lon));

				List<NoteEntry> entries = getNoteEntriesForNote(note.getId());
				note.setEntries(entries);
				notes.add(note);
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			handleError(e);
		} finally {
			connectionManager.freeConnection(con);
		}

		return notes;
	}

	@GET
	@Path("/exception")
	@Produces(MediaType.APPLICATION_JSON)
	public void getException() throws Exception {
		throw new ServiceError("Error Grave");
	}

	public static class ErrorObject {
		Error error;

		public ErrorObject(List<String> errorMessages, String context) {
			error = new Error(context, errorMessages);
		}
	}

	public static class Error {
		String context;
		List<String> errorMessages;

		public Error(String context, List<String> errorMessages) {
			this.context = context;
			this.errorMessages = errorMessages;
		}
	}

	public static class ServiceError extends WebApplicationException {
		private static final long serialVersionUID = 1L;
		private List<String> errors;

		public ServiceError(String... errors) {
			this(Arrays.asList(errors));
		}

		public ServiceError(List<String> errors) {
			// super(Response.status(Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
			// .entity(new GenericEntity<ErrorObject>(new ErrorObject(errors,
			// ""))).build());
			// this.errors = errors;
			super(Response.status(Status.BAD_REQUEST)
					.type(MediaType.APPLICATION_JSON).build());
			this.errors = errors;

		}

		public List<String> getErrors() {
			return errors;
		}
	}

	private String getNotesQuery(int radius, String location) {
		String query = "";
		if (radius == 0 || location == "") {
			query = QueryBuilder.getNotesList();
		} else {
			if (radius != 0 && location != "")
				query = QueryBuilder.getNotesList(radius, new LatLon(location));
		}
		return query;
	}

	protected List<NoteEntry> getNoteEntriesForNote(String noteId) {
		List<NoteEntry> entries = new ArrayList<NoteEntry>();
		ConnectionManager connectionManager = new ConnectionManager();
		Connection con = connectionManager.getConnection();
		String query = QueryBuilder.getNotesEntriesForNote(noteId);
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				NoteEntry entry = new NoteEntry();
				entry.setText(rs.getString(QueryBuilder.ENTRIES_TABLE.TEXT_COL));
				entry.setUserid(rs
						.getString(QueryBuilder.ENTRIES_TABLE.USER_COL));

				entry.setNoteId(rs
						.getString(QueryBuilder.ENTRIES_TABLE.NOTE_COL));

				entries.add(entry);
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			handleError(e);
		}
		connectionManager.freeConnection(con);
		return entries;
	}

	@POST
	@Path("/notes/addentry/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Note addNoteEntry(@PathParam(value = "id") String noteId,
			NoteEntry noteEntry) {
		try {
			ConnectionManager connectionManager = new ConnectionManager();
			Connection con = connectionManager.getConnection();
			String query = QueryBuilder.insertNoteEntryStatement(noteId,
					noteEntry.getText(), noteEntry.getUserid());
			Statement st = con.createStatement();
			st.execute(query);

			return getNoteById(noteId);

		} catch (SQLException e) {
			handleError(e);
		}

		return null;
	}

	@POST
	@Path("/adduser/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public User addUser(User user) {
		//Entity entity = null;
		try {
//			entity = TrackSimpleAPI.createEntity(user.getName());
//			EntityCollection col =  addUserEntityToCollection(entity);
//			if (entity != null && col!= null) {
				//user.setId(entity.getId());
			    user.setId(String.format("user_%s", DateTime.now().getMillis()));
			    ConnectionManager connectionManager = new ConnectionManager();
				Connection con = connectionManager.getConnection();
				String query = QueryBuilder.insertUser(user);
				Statement st = con.createStatement();
				st.execute(query);
				return user;
//			}
//			throw new ServiceError("User entity not created, check api limits");

		} catch (SQLException e) {
			//safeDeleteEntity(entity);
			handleError(e);
		} catch (Exception ex) {
			handleError(ex);
		}

		return null;
	}

//	public static String USERS_COLLECTION = "USERS_COLLECTION";
//
//	EntityCollection usersCollection = null;
//
//	private EntityCollection addUserEntityToCollection(Entity entity) throws IOException, GeneralSecurityException, URISyntaxException{
//		createUsersCollection();
//		if (usersCollection!= null){
//			return TrackSimpleAPI.addEntityToCollection(entity, usersCollection);
//		}
//		return null;
//	}
//	
//	private EntityCollection getUsersCollection() throws IOException, GeneralSecurityException, URISyntaxException {
//		return createUsersCollection();
//	}
//	
//	private EntityCollection createUsersCollection() throws IOException, GeneralSecurityException, URISyntaxException {
//		if (usersCollection == null) {
//			usersCollection = TrackSimpleAPI.getCollectionByName(USERS_COLLECTION);
//            if (usersCollection == null){
//			    usersCollection = TrackSimpleAPI.createCollection(USERS_COLLECTION);
//            }
//		}
//		return usersCollection;
//	}

//	private void safeDeleteEntity(Entity entity) {
//		if (entity != null) {
//			try {
//				TrackSimpleAPI.deleteEntity(entity);
//			} catch (Exception e1) {
//				handleError(e1);
//			}
//		}
//	}

	private Note getNoteById(String noteId) {
		Note note = null;
		ConnectionManager connectionManager = new ConnectionManager();
		Connection con = connectionManager.getConnection();
		try {
			String query = QueryBuilder.getNoteById(noteId);

			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				note = new Note();
				note.setId(rs.getString(QueryBuilder.NOTES_TABLE.NOTE_COL));
				double lat = rs.getDouble(QueryBuilder.NOTES_TABLE.LAT_COL);
				double lon = rs.getDouble(QueryBuilder.NOTES_TABLE.LON_COL);
				note.setLocation(new LatLon(lat, lon));
				List<NoteEntry> entries = getNoteEntriesForNote(note.getId());
				note.setEntries(entries);
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			handleError(e);
		} finally {
			connectionManager.freeConnection(con);
		}
		return note;
	}

	private void handleError(Exception e) {
		throw new ServiceError(e.getMessage());
	}

	@POST
	@Path("/notes/add")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Note addNote(LatLon location) {
		try {
			String id = String.format("NOTE_%s",
					Long.toString(DateTime.now().getMillis()));
			
			ConnectionManager connectionManager = new ConnectionManager();
			Connection con = connectionManager.getConnection();
			String query = QueryBuilder.insertNoteStatement(id,
					location.getLat(), location.getLng());
			Statement st = con.createStatement();
			st.execute(query);
           
//			//if everything is ok create the geofence.
//			GeoFence fence = TrackSimpleAPI.createGeofencesAround(location, 0, String.format("GF_%s", id));
//			EntityCollection collection = getUsersCollection();
//			boolean added = TrackSimpleAPI.addCollectionToGeoFence(collection, fence);
//			
			return getNoteById(id);

		} catch (SQLException e) {
			handleError(e);
		} catch (Exception ex) {
			handleError(ex);
		}
		return null;
	}

	public static List<LatLon> getFenceForPoint(LatLon position) {
		double delta = 0.0001;
		LatLon topLeft = new LatLon(position.getLat() + delta,
				position.getLng() - delta);
		LatLon topRigt = new LatLon(position.getLat() + delta,
				position.getLng() + delta);
		LatLon bottomRight = new LatLon(position.getLat() - delta,
				position.getLng() + delta);
		LatLon bottomLeft = new LatLon(position.getLat() - delta,
				position.getLng() - delta);
		List<LatLon> latLons = new ArrayList<LatLon>();
		latLons.add(topLeft);
		latLons.add(topRigt);
		latLons.add(bottomRight);
		latLons.add(bottomLeft);
		
		return latLons;

	}
	
	@DELETE
	@Path("/notes/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Note removeNote(@PathParam("id") String noteId) {
		try {
			ConnectionManager connectionManager = new ConnectionManager();
			Connection con = connectionManager.getConnection();
			Note note = getNoteById(noteId);
			String query = QueryBuilder.deleteNoteStatement(noteId);
			Statement st = con.createStatement();
			st.execute(query);
			return note;

		} catch (SQLException e) {
			handleError(e);
		}
		return null;
	}

	@POST
	@Path("/user/position/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Note> updateUserPosition(@PathParam("id") String userId,
			LatLon userPosition) {
		
		List<Note> allNotes = getNotes(0, "");
		List<Note> resultNotes = new ArrayList<Note>();
        
		for (Note n: allNotes) {
			LatLon pos = n.getLocation();
			List<LatLon> points = getFenceForPoint(pos);
			BBOX bbox = new BBOX(points);
			if (bbox.isInside(userPosition)){
				resultNotes.add(n);
			}
		}
		
//		List<Crumb> crumbs = new ArrayList<Crumb>();
//		try {
//			boolean recorded = TrackSimpleAPI.recordCrumbs(userId, crumbs);
//			if (recorded){
//				EntityCollection collection = getUsersCollection();
//				List<GeofenceActivityResult> activatedList = TrackSimpleAPI.getActiveGeofences(collection);
//				for (GeofenceActivityResult geofenceResult: activatedList){
//				    if (geofenceResult.getEntityId() == userId) {
//				    	
//				    }
//				}
//			}
//		} catch (Exception e) {
//			
//		}
		
		return resultNotes;
	}

	@GET
	@Path("/users")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getUsers() {
		List<User> users = new ArrayList<User>();
		ConnectionManager connectionManager = new ConnectionManager();
		Connection con = connectionManager.getConnection();
		String query = "";
		query = QueryBuilder.getUsers();

		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getString("id"));
				user.setName(rs.getString("name"));
				users.add(user);
			}
			rs.close();
			st.close();
		} catch (SQLException e) {
			handleError(e);
		} finally {
			connectionManager.freeConnection(con);
		}

		return users;
	}
	
	@GET
	@Path("/notesfences")
	@Produces(MediaType.APPLICATION_JSON)
	public List<NotesFence> getNotesFences() {
		List<NotesFence> fences =  new ArrayList<NotesFence>();
		try {
			List<Note> notes = getNotes(0, "");
//			List<GeoFence> geofences = TrackSimpleAPI.getGeofences();
//			for (GeoFence fence: geofences) {
//				NotesFence nf = new NotesFence();
//				nf.setName(fence.getName());
//				List<LatLon> points = getGeomFromGeofence(fence);
//			    nf.setGeom(points);
//				fences.add(nf);
//			}
			for (Note note : notes) {
				NotesFence nf = new NotesFence();
				nf.setName(note.getId());
				List<LatLon> points = getFenceForPoint(note.getLocation());
				nf.setGeom(points);
				fences.add(nf);
			}
		} catch (Exception e) {
			handleError(e);
		}
		return fences;
	}

//	private List<LatLon> getGeomFromGeofence(GeoFence fence) {
//		if (fence.getPolygon()!= null && fence.getPolygon().getLoops()!= null && fence.getPolygon().getLoops().size()>0){
//		    return fence.getPolygon().getLoops().get(0).getVertices();
//		}
//		return new ArrayList<LatLon>();
//	}
}
