package uji.geotec.geomundus.test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;

import es.uji.geotec.geomundus.common.*;

import junit.framework.TestCase;

public class ServletsTests extends TestCase {

	public static String baseAddress = "http://localhost/webresources/svc/";
	public static String baseAddressDB = "http://localhost/webresources/svc1/";

	public static File webXml = new File(
			"target/geonotes_service/WEB-INF/web.xml");

	@Test
	public void testGetAllNotes() throws IOException, SAXException {
		getAllNotes(baseAddress);
	}

	@Test
	public void testException() throws IOException, SAXException {
		try {
			getException(baseAddressDB);
			fail();
		} catch (Exception e) {
			e.getMessage();
		}
	}

	private void getException(String baseAddress) throws IOException,
			SAXException {
		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();

		String url1 = baseAddress + "exception";

		WebResponse webResponse = client.getResponse(url1); // (3) invoke the
															// servlet w/o
															// authorization
		System.out.println("Response from Server:");
		System.out.println(webResponse.getText());

		checkObjForError(webResponse.getText());

	}

	private List<Note> getAllNotes(String baseAddress) throws IOException,
			SAXException {
		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();

		String url1 = baseAddress + "notes";

		WebResponse webResponse = client.getResponse(url1); // (3) invoke the
															// servlet w/o
															// authorization
		System.out.println("Response from Server:");
		System.out.println(webResponse.getText());

//		ObjectMapper mapper = new ObjectMapper();
//		CollectionLikeType arrayType = mapper.getTypeFactory()
//				.constructCollectionType(ArrayList.class, Note.class);
//
//		@SuppressWarnings("unchecked")
//		List<Note> notes = (List<Note>) mapper.readValue(
//				webResponse.getInputStream(), arrayType);
		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<Note>>(){}.getType();
		List<Note> notes = gson.fromJson(new InputStreamReader(webResponse.getInputStream()), collectionType);
	
		checkObjForError(webResponse.getText());
		return notes;
	}

	@Test
	public void testAddNote() throws IOException, SAXException {
		addNote(baseAddress);
	}

	public Note addNote(String baseAddress) throws IOException,
			SAXException, JsonGenerationException, JsonMappingException {
		LatLon notePosition = new LatLon();
		Note note = addNote(baseAddress, notePosition);
		return note;
	}

	public Note addNote(String baseAddress, LatLon notePosition)
			throws IOException, SAXException, JsonGenerationException,
			JsonMappingException {
		String url = baseAddress + "notes/add";
		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();
		ObjectMapper mapper = new ObjectMapper();
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		mapper.writeValue(outStream, notePosition);
		InputStream source = new ByteArrayInputStream(outStream.toByteArray());
		WebRequest postReq = new PostMethodWebRequest(url, source,
				MediaType.APPLICATION_JSON);
		WebResponse webResponse = client.getResource(postReq);

		Gson gson = new Gson();
		Note note = 
				gson.fromJson(new InputStreamReader(webResponse.getInputStream()), Note.class);
		System.out.println("Response from Server:");
		System.out.println(webResponse.getText());
		//System.out.println("Note:" + note.toString());
		return note;
	}

	@Test
	public void testAddNoteEntry() throws IOException, SAXException {
		Note note = addNote(baseAddress);
		String noteId = note.getId();
		addNoteEntry(baseAddress, noteId, "user1",  "note1 for " + noteId);
		addNoteEntry(baseAddress, noteId, "user1", "note2 for " + noteId);
		addNoteEntry(baseAddress, noteId, "user1","note3 for " + noteId);

	}

	protected Note addNoteEntry(String baseAddress, String noteId, String userId, String text)
			throws IOException, SAXException, JsonGenerationException,
			JsonMappingException {
		String url = baseAddress + "notes/addentry/" + noteId;
		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();

		ObjectMapper mapper = new ObjectMapper();
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		mapper.writeValue(outStream, new NoteEntry(userId, text));

		InputStream source = new ByteArrayInputStream(outStream.toByteArray());
		WebRequest postReq = new PostMethodWebRequest(url, source,
				MediaType.APPLICATION_JSON);
		WebResponse webResponse = client.getResource(postReq);

		Gson gson = new Gson();
		Note note = gson.fromJson(new InputStreamReader(webResponse.getInputStream()), Note.class);

		System.out.println("Response from Server:");
		System.out.println(webResponse.getText());
		return note;
	}

	@Test
	public void testUpdateUserPosition() throws IOException, SAXException {
		String url = baseAddress;
		LatLon p = new LatLon(40.0D, 0.04D);
		updateUserPosition(url, "userid", p);
	}

	protected List<Note> updateUserPosition(String url, String userId,  LatLon p) throws IOException,
			SAXException, JsonGenerationException, JsonMappingException {
		url += String.format("user/position/%s", userId);
		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();

		ObjectMapper mapper = new ObjectMapper();
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		mapper.writeValue(outStream, p);

		InputStream source = new ByteArrayInputStream(outStream.toByteArray());
		WebRequest postReq = new PostMethodWebRequest(url, source,
				MediaType.APPLICATION_JSON);
		WebResponse webResponse = client.getResource(postReq);

		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<Note>>(){}.getType();
		List<Note> notes = gson.fromJson(new InputStreamReader(webResponse.getInputStream()), collectionType);
	
        return notes;		
//		System.out.println("Response from Server:");
//		System.out.println(webResponse.getText());
	}

	@Test
	public void testDeteleNote() throws IOException, SAXException {
		Note note = addNote(baseAddress);
		deleteNote(baseAddress, note.getId());
	}

	protected void deleteNote(String baseAddress, String noteId)
			throws IOException, SAXException {
		String url = baseAddress;
		url += "/notes/" + noteId;

		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();

		WebRequest deleteRequest = new WebRequest(url) {
			@Override
			public String getMethod() {
				return "DELETE";
			}
		};

		WebResponse webResponse = client.getResource(deleteRequest);

		System.out.println("Response from Server:");
		System.out.println(webResponse.getText());
	}

	protected void checkObjForError(String response) {
		if (response.contains("error")) {
			fail();
		}
	}

	public User addUser(String baseAddress, String userName)
			throws IOException, SAXException, JsonGenerationException,
			JsonMappingException {
		String url = baseAddress + "adduser/";
		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();

		ObjectMapper mapper = new ObjectMapper();
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		mapper.writeValue(outStream, new User(userName));

		InputStream source = new ByteArrayInputStream(outStream.toByteArray());
		WebRequest postReq = new PostMethodWebRequest(url, source,
				MediaType.APPLICATION_JSON);
		WebResponse webResponse = client.getResource(postReq);

		User user = mapper.getJsonFactory()
				.createJsonParser(webResponse.getInputStream())
				.readValueAs(User.class);

		System.out.println("Response from Server:");
		System.out.println(webResponse.getText());
		return user;
	}

	@Test
	public void testAddUser() throws IOException, SAXException {
		addUser(baseAddress,
				"ImaginaryUser_"
						+ Long.toString(Calendar.getInstance()
								.getTimeInMillis()));
	}

	@Test
	public void testGetUsers() throws IOException, SAXException {
		getUsers(baseAddress);
	}
	
	protected List<User> getUsers(String baseAddress) throws IOException, SAXException {
		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();

		String url1 = baseAddress + "users";

		WebResponse webResponse = client.getResponse(url1); // (3) invoke the
															// servlet w/o
															// authorization
		System.out.println("Response from Server:");
		System.out.println(webResponse.getText());

//		ObjectMapper mapper = new ObjectMapper();
//		CollectionLikeType arrayType = mapper.getTypeFactory()
//				.constructCollectionType(ArrayList.class, User.class);

		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<User>>(){}.getType();
		List<User> users = gson.fromJson(new InputStreamReader(webResponse.getInputStream()), collectionType);
	
//		@SuppressWarnings("unchecked")
//		List<User> users = (List<User>) mapper.readValue(
//				webResponse.getInputStream(), arrayType);

		checkObjForError(webResponse.getText());
		return users;
	}
	

	@Test
	public void testGetNotesFences() throws IOException, SAXException{
		getNotesFences(baseAddress);
	}   
	
	protected List<NotesFence> getNotesFences(String baseAddress) throws IOException, SAXException {
		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();

		String url1 = baseAddress + "notesfences";

		WebResponse webResponse = client.getResponse(url1); // (3) invoke the
															// servlet w/o
															// authorization
		System.out.println("Response from Server:");
		System.out.println(webResponse.getText());

//		ObjectMapper mapper = new ObjectMapper();
//		CollectionLikeType arrayType = mapper.getTypeFactory()
//				.constructCollectionType(ArrayList.class, User.class);

		Gson gson = new Gson();
		Type collectionType = new TypeToken<ArrayList<NotesFence>>(){}.getType();
		List<NotesFence> result = gson.fromJson(new InputStreamReader(webResponse.getInputStream()), collectionType);
		return result;
//		@SuppressWarnings("unchecked")
//		List<NotesFence> notes = (List<NotesFence>) mapper.readValue(
//				webResponse.getInputStream(), arrayType);
//
		//checkObjForError(webResponse.getText());
	}
	
}
