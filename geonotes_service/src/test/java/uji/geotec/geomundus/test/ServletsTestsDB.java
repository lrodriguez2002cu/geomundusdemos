package uji.geotec.geomundus.test;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;

import es.uji.geotec.geomundus.common.LatLon;
import es.uji.geotec.geomundus.common.Note;
import es.uji.geotec.geomundus.common.User;

public class ServletsTestsDB extends ServletsTests {

	@Test
	public void testGetAllNotesDB() throws IOException, SAXException {

		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();

		String url1 = baseAddressDB + "notes";
		WebResponse webResponse = client.getResponse(url1); // (3) invoke the
															// servlet w/o
															// authorization
		System.out.println("Response from Server:");
		System.out.println(webResponse.getText());

		checkObjForError(webResponse.getText());
	}

	@Test
	public void testGetFilteredNotesDB() throws IOException, SAXException {

		ServletRunner sr = new ServletRunner(webXml);
		ServletUnitClient client = sr.newClient();

		String url1 = baseAddressDB + "notes";// ?radius=0&location=40;0.004";
		GetMethodWebRequest request = new GetMethodWebRequest(url1);
		request.setParameter("radius", "0");
		request.setParameter("location", "40;0.004");

		WebResponse webResponse = client.getResponse(url1); // (3) invoke the
															// servlet w/o
															// authorization
		System.out.println("Response from Server:");
		System.out.println(webResponse.getText());

		checkObjForError(webResponse.getText());
	}

	@Test
	public void testDeleteNoteDB() throws IOException, SAXException {
		Note note = addNote(baseAddressDB);
		deleteNote(baseAddressDB, note.getId());
	}

	@Test
	public void testAddNoteEntryDB() throws IOException, SAXException {
//		Note note = addNote(baseAddressDB);
//		User u = addUser(baseAddressDB, userName)
//		String noteId = note.getId();
//		addNoteEntry(baseAddressDB, noteId, "note1 for " + noteId);
//		addNoteEntry(baseAddressDB, noteId, "note2 for " + noteId);
//		addNoteEntry(baseAddressDB, noteId, "note3 for " + noteId);
	}

	@Test
	public void testAddNoteDB() throws IOException, SAXException {
		addNote(baseAddressDB);
	}

	@Test
	public void testAddUserDB() throws IOException, SAXException {
		addUser(baseAddressDB,
				"ImaginaryUser_"
						+ Long.toString(Calendar.getInstance()
								.getTimeInMillis()));
	}

	@Test
	public void testGetUsers() throws IOException, SAXException {
		getUsers(baseAddressDB);
	}

	@Test
	public void testGetNotesFences() throws IOException, SAXException {
		getNotesFences(baseAddressDB);
	}

	@Test
	public void testUpdateUserPosition() throws IOException, SAXException {
		List<User> users = getUsers(baseAddressDB);
		if (users != null && users.size() > 0) {
			User u = users.get(0);
			String url = baseAddressDB
					;/*+ String.format("user/position/%s", u.getId());*/
			LatLon p = new LatLon(40.0D, 0.04D);
			updateUserPosition(url, u.getId(), p);
		}
	}

	@Test
	public void testAllFlow() throws IOException, SAXException {
		User u = addUser(
				baseAddressDB,
				"NewUser_"
						+ Long.toString(Calendar.getInstance()
								.getTimeInMillis()));
		System.out.println(u);

		Note note = addNote(baseAddressDB, SampleData.getPointInCafeteria());

		note = addNoteEntry(baseAddressDB, u.getId() , note.getId(), "Hello");

		List<Note> notes = updateUserPosition(baseAddressDB, u.getId(), SampleData.getPointInCafeteria());
		System.out.println("Notes: " + notes);
		// testListCollections output: [Collection (name: USERS_COLLECTION, id:
		// 6534f5a2aaef890e, entities: [d61fa3f4f8cd9d8a] )]

		//getNotesFences(baseAddressDB);
	}

}
