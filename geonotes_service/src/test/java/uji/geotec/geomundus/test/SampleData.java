package uji.geotec.geomundus.test;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import es.uji.geotec.geomundus.common.LatLon;
import es.uji.geotec.geomundus.trackclient.Crumb;
import es.uji.geotec.geomundus.trackclient.Geometry;
import es.uji.geotec.geomundus.trackclient.Loop;

public class SampleData {

	public static List<LatLon> getPositions() {
		List<LatLon> list = new ArrayList<LatLon>();
		list.add(new LatLon(39.98877721725515, -0.04770040512084961));
		list.add(new LatLon(39.99051985959892, -0.054931640625));
		list.add(new LatLon(39.99078289609089, -0.05604743957519531));
		list.add(new LatLon(39.99137472449437, -0.058493614196777344));
		list.add(new LatLon(39.99244329056807, -0.0637507438659668));
		list.add(new LatLon(39.99293646927134, -0.06452322006225586));
		list.add(new LatLon(39.99384062097831, -0.06656169891357422));
		return list;
	}

	public static List<Geometry> getSampleGeometries() {

		List<Geometry> sampleGeometries = new ArrayList<Geometry>();

		List<LatLon> cafeteria = getCafeteriaVertices();
		List<LatLon> agora = getAgoraVertices();
		List<LatLon> paraminf = getParaninfVertices();
		
		Geometry geo = getGeometry(cafeteria);
		sampleGeometries.add(geo);

		geo = getGeometry(agora);
		sampleGeometries.add(geo);

		geo = getGeometry(paraminf);
		sampleGeometries.add(geo);

		
		return sampleGeometries;
	}

	public static Geometry  getCafeteriaGeom(){
		List<LatLon> cafeteria = getCafeteriaVertices();
	    Geometry geo = getGeometry(cafeteria);
	    return geo;
	}

	public static Geometry  getAgoraGeom(){
		List<LatLon> agora = getAgoraVertices();
	    Geometry geo = getGeometry(agora);
	    return geo;
	}

	public static Geometry getParaninfGeom(){
		List<LatLon> para = getParaninfVertices();
	    Geometry geo = getGeometry(para);
	    return geo;
	}
	
	public static Geometry getGeometry(List<LatLon> cafeteria) {
		Geometry geo = new Geometry();
		List<Loop> loops = new ArrayList<Loop>();
		Loop loop = new Loop();
		loop.setVertices(cafeteria);
		loops.add(loop);
		geo.setLoops(loops);
		return geo;
	}

	private static List<LatLon> getCafeteriaVertices() {
		List<LatLon> list = new ArrayList<LatLon>();
		list.add(new LatLon(39.99430091179494, -0.06774723529815674));
		list.add(new LatLon(39.994524891472054, -0.06825685501098633));
		list.add(new LatLon(39.99422488182928, -0.06856530904769897));
		list.add(new LatLon(39.99397007808405, -0.06793230772018433));
		//list.add(new LatLon(39.99430091179494, -0.06774723529815674));
		return list;
	}

	private static List<LatLon> getAgoraVertices() {
		List<LatLon> list = new ArrayList<LatLon>();
		list.add(new LatLon(39.99487832579872, -0.06840169429779053));
		list.add(new LatLon(39.995069426155396, -0.06934583187103271));
		list.add(new LatLon(39.99456598858076, -0.06965160369873047));
		list.add(new LatLon(39.99422488182928, -0.06887376308441162));
		//list.add(new LatLon(39.99487832579872, -0.06840169429779053));

		return list;
	}
	
	private static List<LatLon> getParaninfVertices() {
		List<LatLon> list = new ArrayList<LatLon>();
		list.add(new LatLon(39.995509160429016, -0.06778478622436523  ));
		list.add(new LatLon(39.99608450940834, -0.06734490394592285   ));
		list.add(new LatLon(39.996470812431745, -0.06822466850280762  ));
		list.add(new LatLon(39.99578039698033, -0.06864309310913086   ));
		//list.add(new LatLon(39.995509160429016, -0.06778478622436523  ));
		return list;
	}

	public static List<Crumb> getSampleCrumbs() {
		List<Crumb> crumbs = new ArrayList<Crumb>();
		List<LatLon> positions = getPositions();
		DateTime time = new DateTime(DateTimeZone.forID("Europe/Madrid"));
		for (LatLon pos : positions) {
			time = time.plusMinutes(10);
			double secs = time.toInstant().getMillis() / 1000;
			crumbs.add(new Crumb(pos, secs, 10, 90));
		}
		return crumbs;
	}

	public static List<LatLon> getPoligonAtUJI() {
		List<LatLon> list = new ArrayList<LatLon>();
		list.add(new LatLon(39.98877721725515, -0.04770040512084961));
		list.add(new LatLon(39.99051985959892, -0.054931640625));
		list.add(new LatLon(39.99078289609089, -0.05604743957519531));
		list.add(new LatLon(39.99137472449437, -0.058493614196777344));
		list.add(new LatLon(39.99244329056807, -0.0637507438659668));
		list.add(new LatLon(39.99293646927134, -0.06452322006225586));
		list.add(new LatLon(39.99384062097831, -0.06656169891357422));
		return list;
	}

	public static LatLon getPointInCafeteria(){
		return new LatLon(
				39.994243375612506, -0.0680851936340332);
	}
	
	public static LatLon getPointInParaninf(){
		return new LatLon(39.99595300149757, -0.06795644760131836);
	}
	
	public static LatLon getPointInAgora(){
		return new LatLon(39.994687224907366, -0.0691688060760498);
	}
	
}
