package uji.geotec.geomundus.test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Test;
import org.xml.sax.SAXException;

import es.uji.geotec.geomundus.common.LatLon;
import es.uji.geotec.geomundus.trackclient.*;
import es.uji.geotec.geomundus.trackclient.EntityCollection.CollectionList;
import es.uji.geotec.geomundus.trackclient.GeoFence.GeoFencesList;
import es.uji.geotec.geomundus.trackclient.TracksClient.CrumbsServices.DeleteCrumbsParams;
import es.uji.geotec.geomundus.trackclient.TracksClient.CrumbsServices.EntityCrumb;
import es.uji.geotec.geomundus.trackclient.TracksClient.CrumbsServices.HistoryCrumbsParams;
import es.uji.geotec.geomundus.trackclient.TracksClient.CrumbsServices.SummarizeCrumbsParams;
import es.uji.geotec.geomundus.trackclient.TracksClient.GeoFencesServices.ActiveGeofenceParam;
import es.uji.geotec.geomundus.trackclient.TracksClient.GeoFencesServices.GeofencesListParams;
import es.uji.geotec.geomundus.trackclient.TracksClient.GeoFencesServices.GeofencesParams;
import es.uji.geotec.geomundus.trackclient.TracksClient.GeoFencesServices.MemberOperationParam;

import junit.framework.TestCase;

public class TracksClientTests extends TestCase {

	@Test
	public void testCreateEntity() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		String entityName = "Entity1";
		createEntity(entityName);
	}

	public Entity createEntity(String entityName) throws IOException,
			GeneralSecurityException, URISyntaxException {
		return TrackSimpleAPI.createEntity(entityName);
	}

	@Test
	public void testListEntities() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		TrackSimpleAPI.getEntities();
	}

	@Test
	public void testDeleteEntities() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		deleteAllEntities();
	}

	private void deleteAllEntities() throws IOException,
			GeneralSecurityException, URISyntaxException {
		List<Entity> entities = TrackSimpleAPI.getEntities();
		TrackSimpleAPI.deleteEntities(entities);
	}

	@Test
	public void testEntities() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		List<Entity> entities = TrackSimpleAPI.getEntities();
		TracksClient.EntitiesServices.deleteEntities(entities);
		System.out.println(entities.toString());
	}

	@Test
	public void testCreateCollection() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		TrackSimpleAPI.createCollection("Taxi Drivers");
	}

	@Test
	public void testListCollections() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		List<EntityCollection> collections = TrackSimpleAPI.getAllCollections();
		System.out.println("testListCollections output: " + collections);
	}

	@Test
	public void testAddEntityCollections() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		// Get the collections.
		List<EntityCollection> collections = TrackSimpleAPI.getAllCollections();
		Entity entity = createEntity(String.format("Driver_%s",
				Long.toString(Calendar.getInstance().getTimeInMillis())));
		List<Entity> entities = new ArrayList<Entity>();
		entities.add(entity);
		if (collections.size() > 0) {
			EntityCollection collection = collections.get(0);
			TrackSimpleAPI.addEntitiesToCollection(entities, collection);
		}
	}

	@Test
	public void testRemoveEntityFromCollection() throws IOException,
			SAXException, GeneralSecurityException, URISyntaxException {
		// Get the collections.
		List<EntityCollection> collections = TrackSimpleAPI.getAllCollections();
		List<Entity> entities = TracksClient.EntitiesServices.getAllEntities();

		if (collections.size() > 0 && entities.size() > 0) {
			EntityCollection collection = null;
			String entityId = "";
			Entity entity = null;
			for (EntityCollection col : collections) {
				if (col.getEntityIds() != null && col.getEntityIds().size() > 0) {
					collection = col;
					entityId = col.getEntityIds().get(0);
					break;
				}
			}

			for (Entity ent : entities) {
				if (ent.getId().equalsIgnoreCase(entityId)) {
					entity = ent;
					break;
				}
			}

			if (entity != null && collection != null) {
				List<Entity> deleteEntities = new ArrayList<Entity>();
				deleteEntities.add(entity);
				TracksClient.CollectionsServices.removeEntityFromCollection(
						collection, deleteEntities);
				System.out.println("testAddEntityCollections output: "
						+ collection);
			}
		}
	}

	@Test
	public void testDeleteAllCollections() throws IOException,
			GeneralSecurityException, URISyntaxException {
		deleteAllCollections();
	}

	private void deleteAllCollections() throws IOException,
			GeneralSecurityException, URISyntaxException {
		TrackSimpleAPI.deleteAllCollections();
	}

	@Test
	public void testDeleteCollection() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		EntityCollection coll = TrackSimpleAPI
				.createCollection("Collection to delete");
		TrackSimpleAPI.deleteCollection(coll);
	}

	
	@Test
	public void testRecordCrumbs() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		List<Entity> entities = TracksClient.EntitiesServices.getAllEntities();
		if (entities.size() > 0) {
			Entity entity = entities.get(0);
			List<Crumb> crumbs = SampleData.getSampleCrumbs();
			TrackSimpleAPI.recordCrumbs(entity, crumbs);
		}
	}

	@Test
	public void testGetRecentCrumbs() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		List<Entity> entities = TracksClient.EntitiesServices.getAllEntities();
		if (entities.size() > 0) {
			Entity entity = entities.get(0);
			List<Entity> entitiesForColl = new ArrayList<Entity>();
			entitiesForColl.add(entity);

			EntityCollection collection = TrackSimpleAPI
					.createCollection("Collection for testGetRecentCrumbs");
			collection = TracksClient.CollectionsServices
					.addEntityToCollection(collection, entities);

			// List<Crumb> crumbs = getSampleCrumbs();
			List<EntityCrumb> recentCrumbs = TracksClient.CrumbsServices
					.recentCrumbs(collection);
			if (recentCrumbs != null) {
				System.out.println(String.format("Recent entity crumbs: %s ",
						recentCrumbs));
			}
		}
	}

	@Test
	public void testGetHistorycalCrumbs() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		List<Entity> entities = TracksClient.EntitiesServices.getAllEntities();
		if (entities.size() > 0) {
			Entity entity = entities.get(0);
			// Recent entity crumbs: [EntityCrumb(entity: 40b841e4b0c58592,
			// crumb: Crumb(location:
			// es.uji.geotec.geomundus.trackclient.LatLon@34330fb9,
			// timestamp: 2013-11-13T17:08:46.000+01:00)]
			DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
			DateTime time = fmt.parseDateTime("2013-11-13T17:08:46.000+01:00");
			double doubleTime = time.toInstant().getMillis() / 1000;
			List<Crumb> crumbs = TracksClient.CrumbsServices
					.historyCrumbs(new HistoryCrumbsParams(entity.getId(),
							doubleTime, 3, 2));
			System.out.println(String.format("Histotical crumbs: %s", crumbs));

		}
	}

	@Test
	public void testCrumbsSummary() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		List<Entity> entities = TracksClient.EntitiesServices.getAllEntities();
		if (entities.size() > 0) {
			Entity entity = entities.get(0);
			// Recent entity crumbs: [EntityCrumb(entity: 40b841e4b0c58592,
			// crumb: Crumb(location:
			// es.uji.geotec.geomundus.trackclient.LatLon@34330fb9,
			// timestamp: 2013-11-13T17:08:46.000+01:00)]
			DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
			DateTime time = fmt.parseDateTime("2013-11-13T17:08:46.000+01:00");
			double doubleTime = timeToDoubleSecs(time.minusDays(1));
			double doubleTimeAfter = timeToDoubleSecs(time.plusDays(1));

			List<Polyline> crumbs = TracksClient.CrumbsServices
					.sumarizeCrumbs(new SummarizeCrumbsParams(entity.getId(),
							doubleTime, doubleTimeAfter));
			System.out.println(String.format("Summary polyline: %s", crumbs));

		}
	}

	@Test
	public void testDeleteCrumbs() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		List<Entity> entities = TracksClient.EntitiesServices.getAllEntities();
		if (entities.size() > 0) {
			Entity entity = entities.get(0);
			// Recent entity crumbs: [EntityCrumb(entity: 40b841e4b0c58592,
			// crumb: Crumb(location:
			// es.uji.geotec.geomundus.trackclient.LatLon@34330fb9,
			// timestamp: 2013-11-13T17:08:46.000+01:00)]
			DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
			DateTime time = fmt.parseDateTime("2013-11-13T17:08:46.000+01:00");
			double doubleTime = timeToDoubleSecs(time.minusDays(1));
			double doubleTimeAfter = timeToDoubleSecs(time.plusDays(1));

			boolean deleted = TracksClient.CrumbsServices
					.deleteCrumbs(new DeleteCrumbsParams(entity.getId(),
							doubleTime, doubleTimeAfter));
			System.out.println(String.format("Crumbs deleted: %s",
					Boolean.toString(deleted)));

		}
	}

	private double timeToDoubleSecs(DateTime time) {
		double doubleTime = time.toInstant().getMillis() / 1000;
		return doubleTime;
	}

	@Test
	public void testCreateGeofence() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		createSampleGeofences();
	}

	public List<GeoFence> createSampleGeofences() throws IOException,
			GeneralSecurityException, URISyntaxException {
		List<GeoFence> geofences = new ArrayList<GeoFence>();
		geofences.add(new GeoFence("Agora", SampleData.getAgoraGeom()));
		geofences.add(new GeoFence("Paraninf", SampleData.getParaninfGeom()));
		geofences.add(new GeoFence("Cafeteria", SampleData.getCafeteriaGeom()));

		List<GeoFence> result = TrackSimpleAPI.createGeofences(geofences);
		return result;
	}

	@Test
	public void testListGeofences() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		TrackSimpleAPI.getGeofences();
	}

	@Test
	public void testRemoveGeofences() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {

		List<GeoFence> result = TracksClient.GeoFencesServices
				.listGeoFences(new GeofencesListParams());

		if (result.size() > 0) {
			GeofencesParams removeParams = new GeofencesParams(result.get(0));
			boolean deleted = TracksClient.GeoFencesServices
					.removeGeoFences(removeParams);
			System.out.println(String.format("Geofences removed: %s",
					Boolean.toString(deleted)));

			List<GeoFence> resultAfter = TracksClient.GeoFencesServices
					.listGeoFences(new GeofencesListParams());

			assertTrue(result.size() == (resultAfter.size() + 1));
		}
	}

	@Test
	public void testRemoveAllGeofences() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {
		deleteAllGeofences();
	}

	private boolean deleteAllGeofences() throws IOException,
			GeneralSecurityException, URISyntaxException {

		List<GeoFence> result = TracksClient.GeoFencesServices
				.listGeoFences(new GeofencesListParams());

		if (result.size() > 0) {
			GeofencesParams removeParams = new GeofencesParams(result);

			boolean deleted = TracksClient.GeoFencesServices
					.removeGeoFences(removeParams);
			System.out.println(String.format("All Geofences removed: %s",
					Boolean.toString(deleted)));

			List<GeoFence> resultAfter = TracksClient.GeoFencesServices
					.listGeoFences(new GeofencesListParams());
			assertTrue(resultAfter.size() == 0);
			return deleted;
		} else
			return true;
	}

	@Test
	public void testAddMemberToGeofence() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {

		List<GeoFence> existingGeofences = TracksClient.GeoFencesServices
				.listGeoFences(new GeofencesListParams());

		if (existingGeofences.size() == 0) {
			// removeAllGeofences();
			existingGeofences = createSampleGeofences();
		}

		// create an entity
		Entity myself = createEntity("MySelf");

		// create a collection for including myself
		EntityCollection peopleCollection = TrackSimpleAPI
				.createCollection("People Around");

		List<Entity> entities = new ArrayList<Entity>();
		entities.add(myself);

		peopleCollection = TrackSimpleAPI.addEntitiesToCollection(entities,
				peopleCollection);

		if (existingGeofences.size() > 0) {
			GeoFence geofence = existingGeofences.get(0);
			TrackSimpleAPI.addCollectionToGeoFence(peopleCollection, geofence);

		}
	}

	@Test
	public void testRemoveMemberFromGeofence() throws IOException,
			SAXException, GeneralSecurityException, URISyntaxException {

		// get the list of geofences
		List<GeoFence> existingGeofences = TracksClient.GeoFencesServices
				.listGeoFences(new GeofencesListParams());

		if (existingGeofences.size() == 0) {
			// removeAllGeofences();
			existingGeofences = createSampleGeofences();
		}

		// get the collections
		List<EntityCollection> collections = TrackSimpleAPI.getAllCollections();
		EntityCollection peopleCollection = CollectionList.getCollectionByName(
				collections, "People Around");

		if (existingGeofences.size() > 0) {
			GeoFence geofence = existingGeofences.get(0);
			MemberOperationParam removeMemeberParam = new MemberOperationParam(
					geofence,
					CollectionList.getCollectionsIds(peopleCollection));
			boolean removedAdded = TracksClient.GeoFencesServices
					.removeMemeberFromGeoFence(removeMemeberParam);
			System.out.println(String.format("Geofence member removed: %s",
					Boolean.toString(removedAdded)));
		}
	}

	@Test
	public void testActiveGeofence() throws IOException, SAXException,
			GeneralSecurityException, URISyntaxException {

		// get the geofences
		List<GeoFence> existingGeofences = TracksClient.GeoFencesServices
				.listGeoFences(new GeofencesListParams());

		if (existingGeofences.size() == 0) {
			// removeAllGeofences();
			existingGeofences = createSampleGeofences();
		}

		// get all the collections
		List<EntityCollection> collections = TrackSimpleAPI.getAllCollections();

		// get the collection people around
		EntityCollection peopleCollection = CollectionList.getCollectionByName(
				collections, "People Around");
		GeoFence geofence = GeoFencesList.getGeoFenceByName(existingGeofences,
				"Cafeteria");

		if (geofence != null) {
			MemberOperationParam addMemeberParam = new MemberOperationParam(
					geofence,
					CollectionList.getCollectionsIds(peopleCollection));
			boolean removedAdded = TracksClient.GeoFencesServices
					.addMemberToGeoFence(addMemeberParam);

			System.out.println(String.format("Geofence member added: %s",
					Boolean.toString(removedAdded)));
		}

		// Now it send a crumb for checking if the geo-fence is gonna get
		// activated.
		List<String> entityIds = peopleCollection.getEntityIds();
		String entityId = "";

		if (entityIds.size() > 0) {
			entityId = entityIds.get(0);
			List<Crumb> crumbs = new ArrayList<Crumb>();
			/* Get a position in the cafeteria so that it is inside */
			LatLon cafeteriaPos = SampleData.getPointInCafeteria();
			crumbs.add(new Crumb(cafeteriaPos, new DateTime(), 10 /* meters */,
					90));
			boolean crumbRcorded = TrackSimpleAPI
					.recordCrumbs(entityId, crumbs);
			System.out.println(String.format("Crumb recorded: %s",
					Boolean.toString(crumbRcorded)));
		}

		List<GeofenceActivityResult> activeGeofences = TrackSimpleAPI.getActiveGeofences(peopleCollection);

		System.out.println("Active geofences: ");
		for (GeofenceActivityResult activity : activeGeofences) {
			GeoFence activeGeoFence = GeoFencesList.getGeoFenceById(
					existingGeofences, activity.getGeofenceId());
			System.out.println(String.format("name: %s",
					activeGeoFence.getName()));
			System.out.println(activity);
		}

	}
	@Test
	public void testDeleteAll() throws IOException, GeneralSecurityException, URISyntaxException{
		deleteAllEntities();
		deleteAllCollections();
		deleteAllGeofences();
	}

	
}
