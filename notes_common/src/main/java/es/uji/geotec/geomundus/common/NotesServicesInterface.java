package es.uji.geotec.geomundus.common;

import java.util.List;

public interface NotesServicesInterface {

/**	@GET
//	@Path("/notes")
//	@Produces(MediaType.APPLICATION_JSON)*/
	public abstract List<Note> getNotes(/*@QueryParam("radius")*/ int radius,
			/*@QueryParam("location")*/ String location);

/**	@POST
//	@Path("/notes/addentry/{id}")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)*/
	public abstract Note addNoteEntry(/*@PathParam(value = "id") */String noteId,
			NoteEntry noteEntry);

/**	@POST
//	@Path("/notes/add")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON) */
	public abstract Note addNote(LatLon location);

/**	@DELETE
//	@Path("/notes/{id}")
//	@Produces(MediaType.APPLICATION_JSON)*/
	public abstract Note removeNote(/*@PathParam("id")*/ String noteId);

/**	@POST
//	@Path("/user/position/{id}")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)*/
	public abstract List<Note> updateUserPosition(
			/*@PathParam("id")*/ String userId, LatLon userPosition);

/**	@POST
//	@Path("/adduser/")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON) */
	public abstract User addUser(User user);

/**	@GET
//	@Path("/users")
//	@Produces(MediaType.APPLICATION_JSON)*/
	public abstract List<User> getUsers();

	
	/**	@GET
//	@Path("/notesfences")
//	@Produces(MediaType.APPLICATION_JSON)*/

	public abstract List<NotesFence>  getNotesFences();

}