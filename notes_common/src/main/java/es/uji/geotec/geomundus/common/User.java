package es.uji.geotec.geomundus.common;

public class User {
	
	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public User() {
	}
	
	public User(String userName) {
       this.name = userName;
	}
	
	@Override
	public String toString() {
		return String.format("User(id: %s, name: %s)", id, name);
	}
	
}
