package es.uji.geotec.geomundus.common;

import java.util.List;

public class NotesFence {
	
    private  String name;
    private  List<LatLon> geom;
	public List<LatLon> getGeom() {
		return geom;
	}
	public void setGeom(List<LatLon> geom) {
		this.geom = geom;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	} 
}
