package es.uji.geotec.geomundus.common;

public class NoteEntry {
	
	private String userid;
	private String text;
	private String noteId;

	public NoteEntry() {
		this.setUserid("");
		setText("");
	}

	public NoteEntry(String userid, String text) {
		this.setUserid(userid);
		this.setText(text);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	
	public String getNoteId() {
		return noteId;
	}

	public void setNoteId(String noteId) {
		this.noteId = noteId;
	}

	public String toString() {
		return String.format("user: %s, noteId: %s, text: %s", getUserid(), noteId,
				text);
	}

	public String getUserid() {
		return userid;
	}

    public void setUserid(String userid) {
		this.userid = userid;
	}
}
