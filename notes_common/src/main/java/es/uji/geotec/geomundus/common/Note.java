package es.uji.geotec.geomundus.common;

import java.util.ArrayList;
import java.util.List;


public class Note {

	private String id;
	private LatLon location;
	private List<NoteEntry> entries;

	public Note() {
		id = "";
		location = new LatLon();
		entries = new ArrayList<NoteEntry>();
	}

	public Note(String noteId) {
		this();
		this.id = noteId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<NoteEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<NoteEntry> entries) {
		this.entries = entries;
	}

	public LatLon getLocation() {
		return location;
	}

	public void setLocation(LatLon location) {
		this.location = location;
	}

	public String toString() {
		return String.format("Id:%s, location: %s, entries: %s", id,
				location.toString(), entries.toString());
	}
}
