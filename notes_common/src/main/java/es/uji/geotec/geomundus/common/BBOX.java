package es.uji.geotec.geomundus.common;


import java.util.ArrayList;
import java.util.List;

/**
 * Class encapsulating the functionality of a BBOX. Several things are done in this class 
 * and it assures that the BBOX is correctly defined even when the points are not given 
 * in an specific order in the constructor.
 * @author lrodriguez
 *
 */
public class BBOX {

	private LatLon leftTop;
	private LatLon rightBottom;

	public BBOX(List<LatLon> LatLon) {
		if (LatLon != null) {
			for (LatLon c : LatLon) {
				checkCoordinate(c);
			}
		} else {
			throw new RuntimeException("null LatLon");
		}
	}

	public BBOX() {
		// default
	}

	public BBOX(LatLon c1, LatLon c2) {
		checkCoordinate(c1);
		checkCoordinate(c2);
	}

	public void clearBBOX() {
		leftTop = null;
		rightBottom = null;
	}

	public LatLon getRandomPointInside() {
		double baseLat = leftTop.lat;
		double baseLon = leftTop.lng;

		double resultLat = baseLat - (baseLat - rightBottom.lat)
				* Math.random();
		double resultLon = baseLon - (baseLon - rightBottom.lng)
				* Math.random();

		LatLon result = new LatLon(resultLat, resultLon);
		assert isInside(result);
		return result;
	}

	public List<LatLon> getLatLon() {
		List<LatLon> result = new ArrayList<LatLon>();
		if (!isUndefined()) {
			result.add(new LatLon(leftTop));
			result.add(new LatLon(rightBottom));
		}
		return result;
	}

	/**
	 * Returns the LatLon of the bbox considering it as a poligon.
	 * @return
	 */
	public List<LatLon> getPolygonLatLon() {
		List<LatLon> result = new ArrayList<LatLon>();
		result.add(new LatLon(leftTop));
		result.add(new LatLon(leftTop.lat, rightBottom.lng));
		result.add(new LatLon(rightBottom));
		result.add(new LatLon(rightBottom.lat, leftTop.lng));
		return result;
	}

	public void checkLatLon(List<LatLon> coords) {
		if (coords != null) {
			for (LatLon c : coords) {
				checkCoordinate(c);
			}
		}
	}

	public enum CheckMode {
		cmExpand, cmConstrict
	};

	BBOX.CheckMode mode = CheckMode.cmExpand;

	public void setCheckPointMode(BBOX.CheckMode cm) {
		mode = cm;
	}

	private boolean expand() {
		return mode == CheckMode.cmExpand;
	}

	public void checkCoordinate(LatLon c) {

		if ((leftTop == null) && (rightBottom == null)) {
			setLeftTop(new LatLon(c));
			setRightBottom(new LatLon(c));
		}

		leftTop.setLat((expand()) ? Math.max(leftTop.getLat(), c.getLat())
				: Math.min(leftTop.getLat(), c.getLat()));
		/*
		 * if (getLeftTop().getLat() < c.getLat()){
		 * getLeftTop().setLat(c.getLat()); }
		 */

		leftTop.setLng((expand()) ? Math.min(leftTop.getLng(), c.getLng())
				: Math.min(leftTop.getLng(), c.getLng()));
		/*
		 * if (getLeftTop().getLng() < c.getLng()){
		 * getLeftTop().setLon(c.getLng()); }
		 */

		rightBottom.setLat((expand()) ? Math.min(rightBottom.getLat(),
				c.getLat()) : Math.max(rightBottom.getLat(), c.getLat()));

		/*
		 * if (getRightBottom().getLat() > c.getLat()){
		 * getRightBottom().setLat(c.getLat()); }
		 */

		rightBottom.setLng((expand()) ? Math.max(rightBottom.getLng(),
				c.getLng()) : Math.max(rightBottom.getLng(), c.getLng()));

		/*
		 * if (getRightBottom().getLng() > c.getLng()){
		 * getRightBottom().setLon(c.getLng()); }
		 */
	}

	// public void checkCoordinate(LatLon c){
	//
	// if ((leftTop == null) && (rightBottom ==null)){
	// setLeftTop(new LatLon (c));
	// setRightBottom(new LatLon(c));
	// }
	//
	// leftTop.setLat(Math.max(leftTop.getLat(), c.getLat()));
	// /*
	// if (getLeftTop().getLat() < c.getLat()){
	// getLeftTop().setLat(c.getLat());
	// }*/
	//
	// leftTop.setLon(Math.min(leftTop.getLng(), c.getLng()));
	// /*
	// if (getLeftTop().getLng() < c.getLng()){
	// getLeftTop().setLon(c.getLng());
	// }*/
	//
	// rightBottom.setLat(Math.min(rightBottom.getLat(), c.getLat()));
	//
	// /*
	// if (getRightBottom().getLat() > c.getLat()){
	// getRightBottom().setLat(c.getLat());
	// }*/
	//
	// rightBottom.setLon(Math.max(rightBottom.getLng(), c.getLng()));
	// +
	// /*
	// if (getRightBottom().getLng() > c.getLng()){
	// getRightBottom().setLon(c.getLng());
	// }
	// */
	// }

	/**
	 * Indicates when the bbox is defined or not (i.e. the LatLon haven't been defined properly).
	 */
	public boolean isUndefined() {
		return (leftTop == null) || (rightBottom == null)
				|| samePoint(leftTop, rightBottom);
	}

	private boolean samePoint(LatLon a, LatLon b) {
		return (/*a.getAlt() == b.getAlt() && */a.getLat() == b.getLat() && a
				.getLng() == b.getLng());
	}

	/**
	 * Says if a given coordinate is inside the boox.
	 * @param c The coordinate to check.
	 * @return true if the coordinate falls into the bbox, false otherwise.
	 */
	public boolean isInside(LatLon c) {
		if (!isUndefined()) {
			return ((leftTop.lat > c.lat) && (leftTop.lng < c.lng)
					&& (c.lat > rightBottom.lat) && (rightBottom.lng > c.lng));
		} else
			return false;
	}

	private void setLeftTop(LatLon leftTop) {
		this.leftTop = leftTop;
	}

	/**
	 * Gives the left top LatLon of the bbox.
	 * @return
	 */
	public LatLon getLeftTop() {
		return leftTop;
	}

	/**
	 * Sets the right bottom LatLon.
	 * @param rightBottom the LatLon for the right bottom position.
	 */
	private void setRightBottom(LatLon rightBottom) {
		this.rightBottom = rightBottom;
	}

	/**
	 * Gives the right bottom LatLon of the bbox.
	 * @return the LatLon.
	 */
	public LatLon getRightBottom() {
		return rightBottom;
	}
	/**
	 * Gives the center of the bbox by finding the mean in both lat and lon axis.
	 * @return the LatLon of the center.
	 */
	public LatLon getCenter() {
		double baseLat = leftTop.lat;
		double baseLon = leftTop.lng;

		double resultLat = baseLat - (baseLat - rightBottom.lat) * 0.5;
		double resultLon = baseLon - (baseLon - rightBottom.lng) * 0.5;

		LatLon result = new LatLon(resultLat,resultLon);
		assert isInside(result);
		return result;
	}

	/**
	 * Gives the string representation of the bbox in the format (left top (lat;lon), right bottom(lat;lon))
	 */
	public String toString() {
		String result = "";
		if (leftTop != null && rightBottom != null) {
			String sep = ",";
			result = "(" + leftTop.toString() + sep
					+ rightBottom.toString() + ")";
		}
		return result;
	}

	/**
	 * Creates a bbox from a string representation. The string is assumed to have the format:
	 * ((lat; long),(lat;long)) and the outer parenthesis are optional. 
	 * @param bboxStr
	 * @return
	 */
//	public static BBOX fromString(String bboxStr) {
//		BBOX bboxObj = new BBOX();
//		String coords = bboxStr;
//		if (bboxStr != "") {
//			if (bboxStr.startsWith("(") && bboxStr.endsWith(")")) {
//				coords = coords.substring(1, coords.length() - 1);
//			}
//			String[] coordsSequence = coords.split(",");
//
//			for (String bboxCoord : coordsSequence) {
//				LatLon c = LatLon.fromText(bboxCoord);
//				bboxObj.checkCoordinate(c);
//			}
//		}
//		return bboxObj;
//	}
	
	/**
	 * Creates a bounding box based on the specified LatLon
	 * @param bboxStr string containing the bbox.
	 * @param longLatSequence pass true if the order is longitude and latitude, false otherwise.
	 * @return the bbox.
	 */
//	public static BBOX fromString(String bboxStr, boolean longLatSequence) {
//		BBOX bboxObj = new BBOX();
//		String coords = bboxStr;
//		if (bboxStr != "") {
//			if (bboxStr.startsWith("(") && bboxStr.endsWith(")")) {
//				coords = coords.substring(1, coords.length() - 1);
//			}
//			String sep = (coords.contains(","))? ",": (coords.contains(";"))? ";": " ";
//			String[] coordsSequence = coords.split(sep);
//
//			//trim the coordfinates 
//			for (int i =0; i < coordsSequence.length; i++){
//				coordsSequence[i]= coordsSequence[i].trim();
//			}
//			
//			LatLon c = (longLatSequence)? new LatLon(coordsSequence[0], coordsSequence[1]):
//												new LatLon(coordsSequence[1], coordsSequence[0]);
//			bboxObj.checkCoordinate(c);
//			c = (longLatSequence)? new LatLon(coordsSequence[2], coordsSequence[3]):
//				new LatLon(coordsSequence[3], coordsSequence[2]);
//
//			bboxObj.checkCoordinate(c);
//			
//			if (bboxObj.isUndefined()){
//				throw new RuntimeException("Invalid bbox specified. The LatLon for defining the bbox are incomplete");
//			}
//		}
//		return bboxObj;
//	}
}