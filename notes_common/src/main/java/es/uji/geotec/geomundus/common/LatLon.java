package es.uji.geotec.geomundus.common;

public class LatLon {

	double lat;
	double lng;
	

	public LatLon() {
		this.lat = 0;
		this.lng = 0;
	}
	
	public LatLon(String location) {
		String [] parts = location.split(";");
		this.lat = Double.parseDouble(parts[0]);
		this.lng = Double.parseDouble(parts[1]);
	}
	
	public LatLon(LatLon location) {
		this.lat = location.lat;
		this.lng = location.lng;
	}
	
	
	public LatLon(double lat, double lon) {
		this.lat = lat;
		this.lng = lon;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double longitude) {
		this.lng = longitude;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public String toString(){
		return String.format("lat: %s, lon: %s", Double.toString(lat), Double.toString(lng));
	}

}